import pickle
import re
import time
from dataclasses import dataclass, field
from datetime import datetime
from pyfoma import FST
from typing import Callable, Iterable, Optional, BinaryIO, Tuple

@dataclass(frozen=True)
class DependencyDeclaringFunction:
    func: Callable[..., FST]
    vardeps: frozenset[str]
    funcdeps: frozenset[str]

    def __call__(self, *args):
        return self.func(*args)

def depends_on(vardeps: Iterable[str], funcdeps: Iterable[str] = ()) -> Callable[[Callable[..., FST]], DependencyDeclaringFunction]:
    return lambda func: DependencyDeclaringFunction(func, frozenset(vardeps), frozenset(funcdeps))

@dataclass
class Dependencies:
    vars: set[str]
    funcs: set[str]

@dataclass
class CacheEntry:
    definition: str
    dependencies: Dependencies
    last_modified: datetime
    contents: FST

@dataclass
class FuncCacheEntry:
    bytecode: bytes
    consts: tuple
    dependencies: Dependencies
    last_modified: datetime

VAR_USE: re.Pattern = re.compile("\\$(\\w+)")
FUNC_USE: re.Pattern = re.compile("\\$\\^(\\w+)")
BUILTINS: frozenset[str] = frozenset({'reverse', 'invert', 'minimize', 'determinize', 'ignore', 'rewrite', 'restrict', 'project', 'input', 'output', 'rwplus'})

@dataclass
class Cache:
    variables: dict[str, CacheEntry] = field(default_factory=dict)
    funcs: dict[str, FuncCacheEntry] = field(default_factory=dict)

    def get_deps(self, definition: str) -> Dependencies:
        """Gets all of the variables that a given definition depends on."""
        var_deps = {match.group(1) for match in VAR_USE.finditer(definition)}
        func_deps = {match.group(1) for match in FUNC_USE.finditer(definition)} - BUILTINS
        return Dependencies(var_deps, func_deps)
    
    def is_dirty(self, deps: Dependencies, self_last_modified: datetime) -> bool:
        for dependency in deps.vars:
            if dependency not in self.variables:
                return True
            dep_entry = self.variables[dependency]
            if dep_entry.last_modified > self_last_modified:
                return True
        for dependency in deps.funcs:
            if dependency not in self.funcs:
                return True
            dep_entry_f = self.funcs[dependency]
            if dep_entry_f.last_modified > self_last_modified:
                return True
        return False

    def add_def(self, id: str, definition: str, fst: FST):
        """Adds a variable entry to the cache."""
        self.variables[id] = CacheEntry(definition, self.get_deps(definition), datetime.now(), fst)

    def get_cached_fst(self, id: str, s: str) -> Optional[FST]:
        """Retrieves a definition from the cache, if it has not been invalidated."""
        if id not in self.variables:
            return None
        entry = self.variables[id]
        if entry.definition != s:
            return None
        if self.is_dirty(entry.dependencies, entry.last_modified):
            return None
        return entry.contents
    
    def get_or_compute(self, id: str, s: str, compute: Callable[[], FST]) -> Tuple[FST, Optional[float]]:
        cached_def = self.get_cached_fst(id, s)
        if cached_def:
            return cached_def, None
        else:
            timestamp = time.perf_counter()
            result = compute()
            self.add_def(id, s, result)
            return result, time.perf_counter() - timestamp
    
    def add_func(self, func: DependencyDeclaringFunction):
        if not isinstance(func, DependencyDeclaringFunction):
            raise TypeError("Please decorate your method with @depends_on")
        func_id = func.func.__name__
        update = True
        if func_id in self.funcs:
            update = False
            func_entry = self.funcs[func_id]
            if func_entry.bytecode != func.func.__code__.co_code:
                update = True
            if func_entry.consts != func.func.__code__.co_consts:
                update = True
            if self.is_dirty(func_entry.dependencies, func_entry.last_modified):
                update = True
        if update:
            self.funcs[func_id] = FuncCacheEntry(
                func.func.__code__.co_code,
                func.func.__code__.co_consts,
                Dependencies(set(func.vardeps), set(func.funcdeps)),
                datetime.now())

    @classmethod
    def load(cls, read_file: BinaryIO) -> 'Cache':
        cache = pickle.load(read_file)
        assert isinstance(cache, Cache)
        return cache

    def save(self, write_file: BinaryIO):
        pickle.dump(self, write_file, pickle.HIGHEST_PROTOCOL)
