from pyfoma import FST
import prettytable
from prettytable import PrettyTable
from typing import List
import itertools

import v9n

repair_bridge: FST = v9n.defs['RepairBridge']

kappa_ref = {''.join(l[0] for l in word[1]) for word in v9n.defs['Kappa'].words()}
iota_ref = {''.join(l[0] for l in word[1]) for word in v9n.defs['Iota'].words()}

kappa = ["", "s", "r", "n", "þ", "rþ", "l", "t", "c", "f", "ł", "cþ"]
iota = ["", "c", "n", "ŋ", "v", "s", "þ", "š", "r", "l", "ł", "m", "f", "g", "p", "t", "č", "d", "ð", "h", "ħ", "p·", "t·", "d·", "č·", "c·", "g·", "m·", "f·", "v·", "ð·", "cr", "vr", "sr", "þr", "šr", "fr", "gr", "pr", "tr", "dr", "ðr", "hr", "ħr", "p·r", "t·r", "d·r", "c·r", "g·r", "f·r", "v·r", "ð·r", "cl", "vl", "sl", "þl", "šl", "fl", "gl", "pl", "tl", "dl", "ðl", "hl", "ħl", "p·l", "t·l", "d·l", "c·l", "g·l", "f·l", "v·l", "ð·l", "cf", "cþ", "cs", "cš", "gv", "gð", "tf", "dv"]

assert set(kappa) == kappa_ref
assert set(iota) == iota_ref

def f(k, i):
    input = k + ":-" + i
    l = list(repair_bridge.apply(input))
    if len(l) == 0:
        raise AssertionError(f"{input} yielded no output")
    if len(l) > 1:
        raise AssertionError(f"{input} yielded multiple outputs: {l}")
    return l[0]

table1: List[List[str]] = [[f(k, i) for k in kappa] for i in iota]
max_before_colon = 0
max_after_colon = 0
for row in table1:
    for cell in row:
        idx = cell.find(":")
        max_before_colon = max(max_before_colon, idx)
        max_after_colon = max(max_after_colon, len(cell) - idx - 1)

# Print table
table = PrettyTable(field_names=["Onset", *(k or '∅' for k in kappa)])

for (i, row) in itertools.zip_longest(iota, table1):
    row_out = [i or '∅']
    for cell in row:
        idx = cell.find(":")
        row_out.append(" " * (max_before_colon - idx) + cell + " " * (max_after_colon - (len(cell) - idx - 1)))
    table.add_row(row_out)

table.set_style(prettytable.TableStyle.SINGLE_BORDER)

print(table)
