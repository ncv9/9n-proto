# Implementing rewrite rules.

from copy import copy
from dataclasses import dataclass, KW_ONLY
import itertools
from pyfoma import FST
from typing import List, Literal, Tuple
import functools

Order = Literal['simultaneous', 'forward', 'backward', 'outer']

@dataclass
class RewriteRule:
    '''
    An individual rule in a set of parallel rewrite rules.

    Attributes:
        rewrite (FST): The rewrite rule to apply to the center.
        contexts (List[Tuple[FST, FST]]): A list of contexts in which the rewrite rule is applied, expressed as pairs of left and right sides.
        order (Order): The mode of application.
        longest (bool): Prefer longest matches.
        shortest (bool): Prefer shortest matches.
        leftmost (bool | None): Prefer leftmost matches. Defaults to True if order is forward; False otherwise.
        rightmost (bool | None): Prefer rightmost matches. Defaults to True if order is backward; False otherwise.
    '''
    rewrite: FST
    contexts: List[Tuple[FST, FST]]
    order: Order = 'simultaneous'
    _: KW_ONLY
    longest: bool = False
    shortest: bool = False
    # The following default to true if the order is set to a particular value.
    leftmost: bool | None = None
    rightmost: bool | None = None

    def is_leftmost(self) -> bool:
        if self.leftmost is not None: return self.leftmost
        return self.order == 'forward'

    def is_rightmost(self) -> bool:
        if self.rightmost is not None: return self.rightmost
        return self.order == 'backward'

def rewritten(
    rules: List[RewriteRule]
    ):
    '''
    Applies a set of parallel rewrite rules.

    Bugs:
        Matching against multiple contexts is too permissive when the respective rule has an order of forward or backward.
    '''
    n = len(rules)
    if n == 0:
        return FST.re(".*")
    open = [f"'@<{i}@'" for i in range(n)]
    clos = [f"'@>{i}@'" for i in range(n)]
    defs = {f"crossproduct_{i}": rules[i].rewrite for i in range(n)}
    defs['open'] = FST.re("|".join(open))
    defs['clos'] = FST.re("|".join(clos))
    defs['br'] = FST.re("$open|$clos", defs)
    defs['aux'] = FST.re(". - ($br|#)", defs)
    defs['ocbr'] = FST.re("|".join(open[i] + clos[i] for i in range(n)))
    defs['dotted'] = FST.re(".* - (.* $ocbr $ocbr .*)", defs) # Prevent multiple consecutive epentheses
    defs['base'] = FST.re(f"$dotted @ ($aux | {'|'.join(open[i] + f"$crossproduct_{i}" + clos[i] for i in range(n))})*", defs)
    filtered = defs['base']
    for i, rule in enumerate(rules):
        if len(rule.contexts) > 0:
            # Do you know how much grief forgetting   vvv  this star gave me?
            center = FST.re(f"{open[i]} (. - {clos[i]})* {clos[i]}")
            # A lot!
            if rule.order == 'simultaneous':
                lrpairs = ([l.ignore(defs['br']), r.ignore(defs['br'])] for l, r in rule.contexts)
                filtered = center.context_restrict(*lrpairs, rewrite=True).compose(filtered)
            elif rule.order == 'outer':
                lrpairs = ([l.ignore(defs['br']), r.ignore(defs['br'])] for l,r in rule.contexts)
                filtered = filtered.compose(center.context_restrict(*lrpairs, rewrite=True))
                updated_post = True
            else:
                lpairs = [[l.ignore(defs['br']), FST.re(".*")] for l, _ in rule.contexts]
                rpairs = [[FST.re(".*"), r.ignore(defs['br'])] for _, r in rule.contexts]
                left = center.__copy__().context_restrict(*lpairs, rewrite=True)
                right = center.context_restrict(*rpairs, rewrite=True)
                if rule.order == 'forward':
                    filtered = right.compose(filtered).compose(left)
                elif rule.order == 'backward':
                    filtered = left.compose(filtered).compose(right)
                else:
                    raise TypeError(f"dir must be simultaneous, forward, backward, or outer (got {rule.order})")
                updated_pre = (updated_post := True)
        filtered = filtered.epsilon_remove().determinize_as_dfa().minimize()
    defs['rule'] = filtered
    
    # FIXME: Is this correct?
    defs['remrewr'] = FST.re("|".join(f"({open[i]}:'' (.-{clos[i]})* {clos[i]}:'')" for i in range(n))) # worsener
    longest_set = "|".join(open[i] for i, rule in enumerate(rules) if rule.longest)
    shortest_set = "|".join(open[i] for i, rule in enumerate(rules) if rule.shortest)
    leftmost_set = "|".join(open[i] for i, rule in enumerate(rules) if rule.is_leftmost())
    non_simul_leftmost_set = "|".join(open[i] for i, rule in enumerate(rules) if rule.is_leftmost() and rule.order != 'simultaneous')
    rightmost_set = "|".join(clos[i] for i, rule in enumerate(rules) if rule.is_rightmost())
    non_simul_rightmost_set = "|".join(clos[i] for i, rule in enumerate(rules) if rule.is_rightmost() and rule.order != 'simultaneous')
    worseners = [FST.re(".* $remrewr (.|$remrewr)*", defs)]
    if longest_set:
        worseners.append(FST.re(f".* ({longest_set}):$open $aux+ '':($clos $open?) $aux ($br:''|'':$br|$aux)* .*", defs))
    if shortest_set:
        worseners.append(FST.re(f".* ({shortest_set}):$open $aux* $clos:'' $aux+ '':$clos .*", defs))
    if leftmost_set:
        worseners.append(FST.re(\
            f".* ({leftmost_set}):'' $aux+ ('':$open $aux* '':$clos $aux+ $clos:'' .* | '':$open $aux* $clos:'' $aux* '':$clos .*)", defs))
    if non_simul_leftmost_set:
        worseners.append(FST.re(\
            f".* ({non_simul_leftmost_set}):'' $aux+ ($clos:'' $aux+ '':$open $aux* '':$clos .*)", defs))
    if rightmost_set:
        worseners.append(FST.re(\
            f".* ($open:'' $aux+ '':$open $aux* '':$clos | '':$open $aux* $open:'' $aux* '':$clos) $aux+ ({rightmost_set}):'' .*", defs))
    if non_simul_rightmost_set:
        worseners.append(FST.re(\
            f"(.* '':$open $aux* '':$clos $aux+ $open:'') $aux+ ({non_simul_rightmost_set}):'' .*", defs))

    defs['worsen'] = functools.reduce(lambda x, y: x.union(y), worseners).determinize_unweighted().minimize()

    defs['rewr'] = FST.re("$^output($^input($rule) @ $worsen)", defs)
    final = FST.re("(.* - $rewr) @ $rule", defs)
    newfst = final.map_labels({s:'' for s in [*(f'@<{i}@' for i in range(n)), *(f'@>{i}@' for i in range(n)), '#']}).epsilon_remove().determinize_as_dfa().minimize()
    return newfst

def rwplus(*fsts_and_contexts, **flags):
    fsts = []
    order = flags.get('dir', 'simultaneous')
    longest = flags.get('longest', False) == 'True'
    shortest = flags.get('shortest', False) == 'True'
    def get_bflag(s):
        flag = flags.get(s, None)
        if flag is None: return None
        return flag == 'True'
    leftmost = get_bflag('leftmost')
    rightmost = get_bflag('rightmost')
    current = None
    for item in fsts_and_contexts:
        if isinstance(item, FST):
            if current: fsts.append(RewriteRule(*current, order=order, longest=longest, shortest=shortest, leftmost=leftmost, rightmost=rightmost))
            current = (item, [])
        elif isinstance(item, tuple):
            if not current:
                raise TypeError("first argument cannot be context")
            current[1].append(item)
        else:
            raise TypeError(f"expected transducer or context; got {type(item)}")
    if current: fsts.append(RewriteRule(*current, order=order, longest=longest, shortest=shortest, leftmost=leftmost, rightmost=rightmost))
    return rewritten(
        fsts
    )

def rewrite2(fst: FST, *contexts, **flags) -> FST:
    """Returns a modified FST, rewriting self in contexts in parallel, controlled by flags.
    This is a version of PyFoma’s `rewritten` algorithm to
    handle directional application, uwu!
    This doesn’t handle parallel rules, though."""
    order = flags.get('dir', 'simultaneous')
    defs = {'crossproducts': fst}
    defs['br'] = FST.re("'@<@'|'@>@'")
    defs['aux'] = FST.re(". - ($br|#)", defs)
    defs['dotted'] = FST.re(".*-(.* '@<@' '@>@' '@<@' '@>@' .*)")
    defs['base'] = FST.re("$dotted @ # ($aux | '@<@' $crossproducts '@>@')* #", defs)
    if len(contexts) > 0:
        center = FST.re("'@<@' (.-'@>@')* '@>@'")
        if order == 'simultaneous':
            lrpairs = ([l.ignore(defs['br']), r.ignore(defs['br'])] for l,r in contexts)
            defs['rule'] = center.context_restrict(*lrpairs, rewrite=True).compose(defs['base'])
        elif order == 'outer':
            lrpairs = ([l.ignore(defs['br']), r.ignore(defs['br'])] for l,r in contexts)
            defs['rule'] = defs['base'].compose(center.context_restrict(*lrpairs, rewrite=True))
        else:
            contexts = tuple(contexts)
            lpairs = [[l.ignore(defs['br']), FST.re(".*")] for l, _ in contexts]
            rpairs = [[FST.re(".*"), r.ignore(defs['br'])] for _, r in contexts]
            left = center.__copy__().context_restrict(*lpairs, rewrite=True)
            right = center.context_restrict(*rpairs, rewrite=True)
            if order == 'forward':
                defs['rule'] = right.compose(defs['base']).compose(left)
            elif order == 'backward':
                defs['rule'] = left.compose(defs['base']).compose(right)
            else:
                raise TypeError(f"dir must be simultaneous, forward, backward, or outer (got {order})")
    else:
        defs['rule'] = defs['base']

    defs['remrewr'] = FST.re("'@<@':'' (.-'@>@')* '@>@':''") # worsener
    worseners = [FST.re(".* $remrewr (.|$remrewr)*", defs)]
    if flags.get('longest', False) == 'True':
        worseners.append(FST.re(".* '@<@' $aux+ '':('@>@' '@<@'?) $aux ($br:''|'':$br|$aux)* .*", defs))
    if flags.get('leftmost', False) == 'True':
        worseners.append(FST.re(\
             ".* '@<@':'' $aux+ ('':'@<@' $aux* '':'@>@' $aux+ '@>@':'' .* | '':'@<@' $aux* '@>@':'' $aux* '':'@>@' .*)", defs))
        if order != 'simultaneous':
            worseners.append(FST.re(\
             ".* '@<@':'' $aux+ ('@>@':'' $aux+ '':'@<@' $aux* '':'@>@' .*)", defs))
    if flags.get('shortest', False) == 'True':
        worseners.append(FST.re(".* '@<@' $aux* '@>@':'' $aux+ '':'@>@' .*", defs))
    if flags.get('rightmost', False) == 'True':
        worseners.append(FST.re(\
             ".* ('@<@':'' $aux+ '':'@<@' $aux* '':'@>@' | '':'@<@' $aux* '@<@':'' $aux* '':'@>@') $aux+ '@>@':'' .*", defs))
        if order != 'simultaneous':
            worseners.append(FST.re(\
             "(.* '':'@<@' $aux* '':'@>@' $aux+ '@<@':'') $aux+ '@>@':'' .*", defs))
    defs['worsen'] = functools.reduce(lambda x, y: x.union(y), worseners).determinize_unweighted().minimize()

    defs['rewr'] = FST.re("$^output($^input($rule) @ $worsen)", defs)
    final = FST.re("(.* - $rewr) @ $rule", defs)
    newfst = final.map_labels({s:'' for s in ['@<@','@>@','#']}).epsilon_remove().determinize_as_dfa().minimize()
    return newfst
