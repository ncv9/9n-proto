# Implementation of Ŋarâþ Crîþ v9n’s morphophonological rules using PyFoma (https://github.com/mhulden/pyfoma/)

import copy
import pickle
import re
import sys
import unittest
from pyfoma import FST, Paradigm, State
from typing import Callable, Optional, Tuple
from fstcache import *
import rwplus

CACHE_PATH = "cache.pkl"
cache: Cache = Cache()
try:
    with open(CACHE_PATH, "rb") as read_file:
        cache = Cache.load(read_file)
except Exception as e:
    print(f"Failed to read cache ({e}); starting anew", file=sys.stderr)
    pass

# We maintain a global list of FSM definitions and define a helper to add a variable to be used by the `FST` constructor:

defs: dict[str, FST] = {} # List of existing definitions
functions: set[Callable[..., FST]] = {rwplus.rwplus} # List of functions

def dd(id: str, s: str):
    """Adds a definition to defs with the existing definitions in scope."""

    defs[id], time = cache.get_or_compute(id, s, lambda: FST.re(s, defs, functions=functions))
    if time is None:
        print(f"{id} => {len(defs[id])} states (cached)")
    else:
        print(f"{id} => {len(defs[id])} states ({time:.2f}s)")

def add_funcs(*funcs: DependencyDeclaringFunction):
    for func in funcs:
        cache.add_func(func)
        functions.add(func.func)

EMPTY = FST.re("a&e")

# Syllable components

# We begin by describing the glide, vowel, and coda:

dd('Mu', "j?")
dd('Nu', "[eoaîiêôâu]")
dd('MN', "$Mu$Nu")
dd('Kappax', "[srnþltcfł]|rþ|cþ")
dd('Kappa', "$Kappax?")
dd('Omega', "$Kappa|st|lt|ns|ls|nþ|łt|m")

# Now we define variables for the onset. First, we define the consonant classes and the list of special initials:

dd('Sigma0', "[cnŋvsþšrlłmfgptčdðhħ]")
dd('Sigma1', "[cvmfgptčdð]·")
dd('Sigma2', "nd|ŋg|vf|vp|lł|mp|gc|dt|ðþ|G")
dd('Sigma_iota', "$Sigma0|$Sigma1")
dd('Sigma_alpha', "$Sigma_iota|$Sigma2")

dd('Pi0', "[cvsþšfgptdðhħ]")
dd('Pi_iota', "$Sigma_iota&($Pi0.*)")
dd('Pi_alpha', "$Pi_iota|nd|ŋg|vf|vp|mp|gc|dt|ðþ")

dd('Rho', "[lr]")

dd('Lambda', "cs|cþ|cš|cf|gv|gð|tf|dv")

# Now we define the set of all initials:

dd('Iota', "($Sigma_iota | ($Pi_iota $Rho) | $Lambda)?")
dd('Alpha', "($Sigma_alpha | ($Pi_alpha $Rho) | $Lambda)?")

# The cþenros

# We now build up the state machine. We do this explicitly instead of using regular expressions so that we can begin and end on any of the six named states. We first create the named states:

cþenros: FST = FST()
alpha = cþenros.initialstate
alpha.name = "alpha"
s = State(name="s")
g = State(name="g")
o = State(name="o")
n = State(name="n")
omega = State(name="omega")
cþenros.states.update({s, g, o, n, omega})

# Now add the transitions:

# Adapated from the PyFoma source code: https://github.com/mhulden/pyfoma/blob/main/src/pyfoma/algorithms.py#L24
def harmonize_alphabet(fst1: FST, fst2: FST) -> Tuple[FST, FST]:
    for A, B in [(fst1, fst2), (fst2, fst1)]:
        if '.' in A.alphabet and (A.alphabet - {'.'}) != (B.alphabet - {'.'}):
            Aexpand = B.alphabet - A.alphabet - {'.', ''}
            if A == fst2:
                A, _ = fst2.copy_filtered()
                fst2 = A # Need to copy to avoid mutating other
            for s, l, t in list(A.all_transitions(A.states)):
                if '.' in l:
                    for sym in Aexpand:
                        newl = tuple(lbl if lbl != '.' else sym for lbl in l)
                        s.add_transition(t.targetstate, newl, t.weight)
    return (fst1, fst2)

def add_link(cþenros: FST, x: State, y: State, Xi: FST):
    Xi, _ = Xi.copy_filtered()
    _, Xi = harmonize_alphabet(cþenros, Xi)
    cþenros.states.update(Xi.states)
    x.add_transition(Xi.initialstate, '', 0.0)
    for final_state in Xi.finalstates:
        final_state.add_transition(y, '', 0.0)
    # Erase the names of Xi.states so node names don’t alias and impede debugging
    for state in Xi.states:
        state.name = None
    cþenros.alphabet.update(Xi.alphabet)

add_link(cþenros, alpha, g, defs['Alpha'])
add_link(cþenros, s, g, defs['Iota'])
add_link(cþenros, g, o, defs['Mu'])
add_link(cþenros, o, n, defs['Nu'])
# `*` mutates the left FST in place for some reason, so copy it
# to avoid mutating our copy.
add_link(cþenros, n, s, copy.copy(defs['Kappa']) * FST((':',)))
add_link(cþenros, n, omega, defs['Omega'])

# Note that we explicitly separate syllables using `:` to make it easier to match on specific codas. Once hyphens are admitted, syllabic morpheme boundaries are always spelled as `:-` rather than `-:`.

# We create a new version of `cþenros` that allows for hyphens:

cþenros_hyph = cþenros
cþenros = copy.copy(cþenros)
cþenros_hyph.alphabet.update({'-', '[-j]'})
# add_transition expects a tuple of symbols for the label argument.
# If we pass in a plain string, then it will interpret it as
# '[':'-':-'j':']' instead of a single multi-character symbol.
s.add_transition(s, ('-',), 0.0)
g.add_transition(g, ('-',), 0.0)
o.add_transition(o, ('-',), 0.0)
o.add_transition(o, ('[-j]',), 0.0)
n.add_transition(n, ('-',), 0.0)

# Now we can create $St(x, y)$ and $Stx(x, y)$ for any combination of $x$ and $y$ we need:

def simplify(fst):
    return fst \
        .trim() \
        .epsilon_remove() \
        .determinize_as_dfa() \
        .minimize_as_dfa() \
        .label_states_topology() \
        .cleanup_sigma()

def dst(x: str, y: str):
    stx = copy.copy(cþenros)
    st = copy.copy(cþenros_hyph)
    for state in stx.states:
        if state.name == x:
            stx.initialstate = state
        elif state.name == y:
            stx.finalstates = {state}
    for state in st.states:
        if state.name == x:
            st.initialstate = state
        elif state.name == y:
            st.finalstates = {state}
    xy = x[0] + (y[0] if y != "omega" else "w")

    HACK_DEF = "$Alpha$Iota$Mu$Nu$Kappa$Omega"
    defs["Sx_" + xy] = cache.get_or_compute("Sx_" + xy, HACK_DEF, lambda: simplify(stx))[0]
    defs["S_" + xy] = cache.get_or_compute("S_" + xy, HACK_DEF, lambda: simplify(st))[0]

dst("alpha", "omega")

# Cþencolar

# Check for oginiþe cfarðerþ

@depends_on(("MN",))
def class_iplus_oc(cc: FST) -> FST:
    '''Given an FSM that describes all consonants of a certain
    consonant class, returns an FSM that matches all strings that
    contain class I+ oginiþe cfarðerþ across a hyphen.'''
    return FST.re("$cc r? $MN $cc | $cc $MN r $cc", { **defs, 'cc': cc })
  
@depends_on(("MN",))
def class_i_oc(cc: FST) -> FST:
    '''Given an FSM that describes all consonants of a certain
    consonant class, returns an FSM that matches all strings that
    contain class I oginiþe cfarðerþ across a hyphen.'''
    return FST.re("$cc $MN $cc", { **defs, 'cc': cc })
  
@depends_on(("MN",))
def class_ii_oc(cc: FST) -> FST:
    '''Given an FSM that describes all consonants of a certain
    consonant class, returns an FSM that matches all strings that
    contain class II oginiþe cfarðerþ across a hyphen.'''
    # pattern = EMPTY # Rejects all strings
    # for v in "eoaîiêôâu":
    #     pattern = FST.re(f"$pattern | $cc $Mu {v} $cc $Mu {v} | $Mu {v} $cc $Mu {v} $cc", { **defs, 'pattern': pattern, 'cc': cc})
    return FST.re("$cc $MN $cc $MN | $MN $cc $MN $cc", { **defs, 'cc': cc})

add_funcs(class_iplus_oc, class_i_oc, class_ii_oc)

dd(
    'TypeIOC',
    """
    ($^ignore(
        $^class_iplus_oc(þ|t·|ðþ) |
        $^class_iplus_oc(ð|d·|ðþ|ð·) |
        $^class_i_oc(c|c·|gc) |
        $^class_i_oc(ŋ|ŋg) |
        $^class_i_oc(v|m·|vf|vp|v·) |
        $^class_i_oc(š|č·) |
        $^class_i_oc(m|mp|m·) |
        $^class_i_oc(f|p·|f·|vf) |
        $^class_i_oc(g|gc|G|g·|ŋg) |
        $^class_i_oc(p|p·|vp|mp) |
        $^class_i_oc(č|č·) |
        $^class_i_oc(d|dt|d·|nd) |
        $^class_i_oc(h|c·) |
        $^class_i_oc(ħ|g·) |
        $^class_ii_oc(n|nd) |
        $^class_ii_oc(s) |
        $^class_ii_oc(l|lł) |
        $^class_ii_oc(t|t·|dt),
        '-'|':'
    ) & ((. - '-') .* '-' .* (. - '-')))
    """
)

@depends_on(("Iota", "Mu"))
def type_ii_oc(vc: FST) -> FST:
    pattern = EMPTY
    for k in ["c", "cþ", "n", "s", "þ", "r", "rþ", "l", "ł", "f", "t"]:
        pattern = FST.re(f"$pattern | $vc {k} ':' $Iota $Mu $vc {k}", { **defs, 'vc': vc, 'pattern': pattern })
    return pattern

add_funcs(type_ii_oc)

dd(
    'TypeIIOC',
    """
    ($^ignore(
        $^type_ii_oc([eê]) |
        $^type_ii_oc([oô]) |
        $^type_ii_oc([aâ]) |
        $^type_ii_oc([iî]) |
        $^type_ii_oc([u]),
        '-'
    ) & ((. - '-') .* '-' .* (. - '-')))
    """
)

@depends_on(("Nu",))
def type_iii_oc(vc: FST) -> FST:
    return FST.re("$vc $Kappa ':' $Iota $Mu $vc $Kappa ':' $Iota $Mu $vc", { **defs, 'vc': vc })

add_funcs(type_iii_oc)

dd(
    'TypeIIIOC',
    """
    ($^ignore(
        $^type_iii_oc([eê]) |
        $^type_iii_oc([oô]) |
        $^type_iii_oc([aâ]) |
        $^type_iii_oc([iî]) |
        $^type_iii_oc([u]),
        '-'
    ) & ((. - '-') .* '-' .* (. - '-')))
    """
)

# We restrict matches to $S(alpha, omega)$; otherwise, building the
# automaton takes too long.
dd('HasTypeIOC', "$S_aw & .* $TypeIOC .*")
dd('HasTypeIIOC', "$S_aw & .* $TypeIIOC (':' .*)?")
dd('HasTypeIIIOC', "$S_aw & .* $TypeIIIOC .*")

# dd('HasTypeIOrIIOC', "$HasTypeIOC | $HasTypeIIOC")

# dd(
#     'HasOC',
#     """
#     $HasTypeIOrIIOC | $HasTypeIIIOC
#     """
# )

# Bridge canonicalization & repair

@depends_on(())
def priority_union(*xs: FST) -> FST:
    """
    Returns the priority union of a number of transducers.
    This functions similarly to the union operator, except that in
    the case of conflicting outputs for an input, the first
    transducer that produces an output takes precedence.
    It is implemented as the .P. operator in Foma.
    """
    result = EMPTY
    for x in reversed(xs):
        # Work around shitass PyFoma bug with ~$^input('':fuck you)
        x_in = FST.re("$^input($x)", { 'x': x })
        result = FST.re("$x | (~$x_in @ $y)", { 'x': x, 'x_in': x_in, 'y': result })
    return result

@depends_on((), ("priority_union",))
def bilenient_compose(*xs: FST) -> FST:
    """
    Bidirectional lenient composition ($^priority_union($x @ $y, $x, $y)).
    Note that this acts differently from Foma’s .O. operator,
    which is defined as $^priority_union($x @ $y, $x).
    """
    result = xs[0]
    for x in xs[1:]:
        result = FST.re("$^priority_union($x @ $y, $x, $y)", { 'x': result, 'y': x }, {priority_union.func})
    return result

@depends_on((), ('priority_union',))
def passthru(x: FST) -> FST:
    return FST.re("$^priority_union($x, .*)", {'x': x}, functions={priority_union.func})

dd('CollapseHyphensB', "$^rewrite('-':'' / _ ('-'|':'))")

@depends_on(('CollapseHyphensB',), ('passthru',))
def bridgerule(x: FST) -> FST:
    p = FST.re("('-'? $x '-'?) @ $CollapseHyphensB", {**defs, 'x': x})
    return FST.re("$^passthru($p)", {'p': p}, functions={passthru.func})

add_funcs(priority_union, passthru, bridgerule)

dd(
    'CanonicalizeBridge',
    # All codas have at most two consonants, so we can try grouping
    # with zero, one, or two consonants in the coda, in that order.
    """
    (
        ($Kappa ':' $Iota) @
        $^rewrite(':':'') @
        $^priority_union(
            ('':':') $Iota,
            . ('':':') $Iota,
            . . ('':':') $Iota
        )
    )
    """
)
dd('CanonicalBridge', "$^output($CanonicalizeBridge)")

# Further bridge repair rules lifted from v9e.

# TODO: How should t:šr and t:šl be treated?
# We can’t coalesce them into :čr and :čl because č is not an EPF.
dd('CoalesceAffricate', "$^passthru('-'? t:'' ':' '-' š:č ('-'?:'-'))")
dd(
    'FortifyH',
    """
    $^bridgerule((s|þ|rþ|t|c|f|ł|cþ) ':' '-' h:c (l|r)? '':'-')
    """
)
dd(
    'FortifyĦ',
    """
    $^bridgerule((t|c|f|ł) ':' '-' ħ:g (l|r)? '':'-')
    """
)
dd(
    'MetathesizeTWithVelar',
    """
    $^bridgerule(
        '':'-' (
            ((t ':' '-' c):(c ':' '-' t)) | ((t ':' '-' g):(c ':' '-' d))
        ) '·'? (l|r)? '':'-'
    )
    """
)
dd(
    'MetathesizeTWithVelarCluster',
    """
    $^bridgerule(
        (t:'') ':' '-' ((cf|cþ|cs|cš) '-'?|((gv):(cf '-'))|((gð):(cþ '-')))
    )
    """
)
dd(
    'AssimilateNasals',
    """
    $^bridgerule(
        $^priority_union(
            '-'? (c:'') ':' '-' ŋ '-'?,
            ('-'?:'-') (t:n | c:ŋ) ':' '-' (m|n|ŋ|m·) '-'?
        )
    )
    """
)
dd(
    'DenasalizeŊ',
    """
    $^bridgerule('-'? (s|þ|rþ|f|ł|cþ) ':' '-' (ŋ:g) ('-'?:'-'))
    """
)
dd(
    'FortiteOnsetsAfterŁ',
    """
    $^bridgerule(
        '-'? ł ':' '-' ((þ|t·|s):t | (ð|d·):d) (l|r)? ('-'?:'-')
    )
    """
)
dd(
    'DevoiceVÐ',
    # For assimilation of ð· after the preceding consonant,
    # we use a special symbol so it doesn’t get mangled by
    # the next step.
    """
    $^bridgerule(
        $^priority_union(
            (þ:'') ':' '-' v (l|r),
            '':'-' (rþ):l ':' '-' (v|ð) l:'' '':'-',
            '':'-' (rþ):r ':' '-' (v|ð) r:'' '':'-',
            (þ|rþ|t|c|f|cþ) ':' '-' v:f '·'? (l|r)? '':'-',
            (þ|rþ|t|c|f|cþ|s) ':' '-' ð:þ (l|r)? '':'-',
            ł ':' '-' (ð·):'' (l|r) '':'-',
            (þ|rþ|t|c|f|cþ|s) ':' '-' (ð·):'[Copy]' (l|r)? '':'-'
        )
    )
    """
)
dd(
    'AssimilateSAfterÞ',
    """
    $^bridgerule(
        $^priority_union(
            (~'-')* þ ':' '-' s:þ '':'-',
            s:'' ':' '-' s:þ [rl]? '':'-',
            s:'' ':' '-' '[Copy]':þ [rl] '':'-'
        )
    )
    """
)
dd(
    'RealizeCopySymbol',
    """
    $^bridgerule(
        $^priority_union(
            .* þ ':' '-' '[Copy]':þ .*,
            .* t ':' '-' '[Copy]':t .*,
            .* c ':' '-' '[Copy]':c .*,
            .* f ':' '-' '[Copy]':f .*,
            .* s ':' '-' '[Copy]':s .*
        )
    )
    """
)
dd(
    'ReplaceŁCoda',
    """
    $^bridgerule(
        '':'-' ł:l ':' '-' ($Lambda | ~([tdlrnc] .* | ''))
    )
    """
)
dd(
    'DegeminateConsonants',
    """
    $^bridgerule(
        $^priority_union(
            (('':'-').+)? þ:'' ':' '-' þ '·'? $Sigma0,
            (('':'-').+)? t:'' ':' '-' (t|d) '·'? $Sigma0,
            (('':'-').+)? c:'' ':' '-' (c|g) '·'? $Sigma0,
            (('':'-').+)? f:'' ':' '-' f '·'? $Sigma0
        )
    )
    """
)
dd(
    'DegeminateThroughPlosive',
    """
    $^bridgerule(
        $^priority_union(
            f ':' '-' [ct] (f:'') '':'-',
            þ ':' '-' c (þ:'') '':'-',
            s ':' '-' c (s:'') '':'-'
        )
    )
    """
)
dd(
    'ElideTwoConsonantCodasPartially',
    """
    $^bridgerule(
        $^priority_union(
            '':'-' r (þ:'') ':' '-' ([fvþðsšhħ] '·'? [lr] | cf | cþ | cs | cš | tf | dv),
            '':'-' (r:'') þ ':' '-' $Sigma0 '·'? $Sigma0,
            cþ ':' '-' (þ|š|r|l|m|t|ħ|m·|t·)?,
            cþ ':' '-' (
                ((cs):þ | c:'' [fþš] | t:'' f | [þšħ]:'' [lr])
            ) '':'-',
            '':'-' (c:'') þ ':' '-' $Iota
        )
    )
    """
)

# Put it all together

dd(
    'RepairBridge',
    """
    ($Kappa ':' '-' $Iota) @
    $CoalesceAffricate @
    $FortifyH @ $FortifyĦ @
    $MetathesizeTWithVelar @ $MetathesizeTWithVelarCluster @
    $AssimilateNasals @
    $DenasalizeŊ @
    $FortiteOnsetsAfterŁ @
    $DevoiceVÐ @
    $AssimilateSAfterÞ @ $RealizeCopySymbol @
    $ReplaceŁCoda @
    $DegeminateConsonants @
    $DegeminateThroughPlosive @
    $ElideTwoConsonantCodasPartially
    """
)
dd('RepairBridgeRw', "$^rewrite($RepairBridge)")

dd('ValidBridge', "$^output($RepairBridge)")
dd('HasOnlyValidBridges', "$Alpha '-'? ($Mu '-'? $Nu '-'? ($ValidBridge | $Kappa ':' $Iota) '-'?)* $Mu '-'? $Nu '-'? $Omega")

# FST.render(defs['RepairBridge'])

# Sound changes

# Resolve [-j]
dd('CollapseHyphensJ', "$^rewrite('-':'' / _ ('-'|'[-j]')) @ $^rewrite('[-j]':'' / _ '[-j]')")
dd('ResolveHyphJ', "$CollapseHyphensJ @ $^rewrite(j '[-j]':'-') @ $^rewrite('[-j]':('-' j))")

# Remove j before i, î, or u
dd('RemoveJBeforeI', "$^rewrite(j:'' / _ '-'? [iîu])")

# Type I deduplication rules
dd('CollapseHyphens', "$^rewrite('-':'' / _ '-')")
dd('InsertFreeHyphens', "$^rewrite('':?'-')")

@depends_on(('InsertFreeHyphens',))
def at_least_one_hyphen_in(fst: FST) -> FST:
    """
    Given an FSM, return an FSM that contains strings which intersperse
    at least one hyphen among the characters, such that at most one
    hyphen is inserted at each possible point. For example,
    {ab, cde} would contain a-b, c-de, c-d-e-, and -ab, but not
    ab or a--b.
    """
    # Fucking shitass PyFoma bug strikes again
    fst2 = FST.re('$fst @ $InsertFreeHyphens', {**defs, 'fst': fst})
    fst3 = FST.re('$^output($fst2)', {'fst2': fst2})
    return FST.re("$fst3 & (.* '-' .*)", {'fst3': fst3})

add_funcs(at_least_one_hyphen_in)

dd('MNh', "$^at_least_one_hyphen_in($MN ':'?)")
dd('MNhr', "$^at_least_one_hyphen_in($MN r? ':'?)")
dd('MNrh', "$^at_least_one_hyphen_in(r $MN ':'?)")
dd('MNhx', "$MNh - ('-' .*)")

dd(
    'DeduplicateTypeIOCMedial',
    # We can defer hyphen collapses until all medial type I OC has
    # been resolved because multiple hyphens will only appear in
    # areas we no longer care about.
    """
    $^rwplus(
        ('-'?:'-') (þ:t) / ':' _ $MNhr (þ|t·),
        (':' '-'? cþ):(c ':' '-' t) / $Nu '-'? _ $MNhr (þ|t·),
        ('-'?:'-') ((cþ):t) / $Kappax ':' _ $MNhr (þ|t·),
        ('-'? þ):('-' t) / $Nu _ ':' $MNhr (þ|t·),
        (þ ':'):(':' '-' t) / [cr] _ ($MNhr|$MNrh) (þ|t·),
        ('-'?:'-') ((t·):t) / ':' _ $MNhr (þ|t·),
        ('-'?:'-') (þr):(tr) / (. - t) ':' _ $MNh (þ|t·),
        (t:'') ':' ('-'?:'-') (þr):(tr) / _ $MNh (þ|t·),
        ('-'? þ):('-' t) / $Nu _ ':' $MNrh (þ|t·),
        ('-'?:'-') (t·r):(tr) / ':' _ $MNh (þ|t·),
        ('-'?:'-') (ð|gð|ð·|d·|ðr|ð·r|d·r):ŋ / ':' _ $MNhr (ð|ð·|d·)
        , dir = backward, rightmost = True 
    ) @ $CollapseHyphens
    """
)
# FST.render(defs['DeduplicateTypeIOCMedial'])

# Put it all together
dd('Apply',
    """
    $ResolveHyphJ @ $RemoveJBeforeI @
    $RepairBridgeRw @
    $DeduplicateTypeIOCMedial
    """
)
dd('Applied', "$S_aw @ $Apply")

with open(CACHE_PATH, "wb") as write_file:
    cache.save(write_file)
