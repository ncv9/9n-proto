
from pyfoma import FST, Paradigm, State
import itertools

import pytest
import rwplus
import v9n

# Helper functions for testing

def ff(fst):
    """If fst is a string, then treat it as a key to one of the definitions in `v9n.defs`.
    Otherwise, return it unchanged."""
    if isinstance(fst, str):
        return v9n.defs[fst]
    return fst

def accepts(fst, input):
    """Returns true if `fst` accepts `input`.
    If `fst` is a string, it is treated as the name of an item in `defs`."""
    fst = ff(fst)
    return next(fst.apply(input), None) is not None

def rejects(fst, input):
    """Returns true if `fst` *rejects* `input`."""
    return not accepts(fst, input)

def test_accepts_rejects():
    assert accepts(FST.re("a+"), "a")
    assert accepts(FST.re("a+"), "aa")
    assert rejects(FST.re("a+"), "")
    assert rejects(FST.re("a+"), "b")
    assert rejects(FST.re("a+"), "ab")


def assert_contains(aa, bb):
    """Asserts that `a` contains all strings in `b`.
    This is an assertion rather than a predicate for better messages when the assertion is violated."""
    a = ff(aa)
    b = ff(bb)
    # Needs to be simplified or else violations.words() might loop infintely!
    violations = v9n.simplify(b - a)
    first_violation = next(violations.words(), None)
    if first_violation is not None:
        raise AssertionError(f"{first_violation} is in {bb} but not in {aa}")

def assert_disjoint(aa, bb):
    """Asserts that `a` and `b` have no strings in common.
    This is an assertion rather than a predicate for better messages when the assertion is violated."""
    a = ff(aa)
    b = ff(bb)
    # Needs to be simplified or else violations.words() might loop infintely!
    violations = v9n.simplify(a & b)
    first_violation = next(violations.words(), None)
    if first_violation is not None:
        raise AssertionError(f"{first_violation} is in both {aa} and {bb}")

def test_contains():
    assert_contains(FST.re("a*"), FST.re("a+"))
    assert_contains(FST.re("a+b+a+"), FST.re("aaaaa?ba?a"))

def test_not_contains():
    with pytest.raises(AssertionError):
        assert_contains(FST.re("a+"), FST.re("a*"))

def test_disjoint():
    assert_disjoint(FST.re("a*"), FST.re("ab+a"))

def test_not_disjoint():
    with pytest.raises(AssertionError):
        assert_contains(FST.re("a*"), FST.re("ab*a"))

def forward(fsts, input):
    """Returns the unique output when `fst` takes `input`.
    An assertion error will be raised if the input is not accepted
    or results in ambiguous output."""
    fst = ff(fsts)
    gen = fst.generate(input)
    first = next(gen, None)
    if first is None:
        raise AssertionError(f"{input} is not accepted by {fsts}")
    second = next(gen, None)
    if second is not None:
        raise AssertionError(f"Multiple outputs for {fsts} on {input}: {first} and {second}")
    return first

def test_forward():
    woofify = FST.re("$^rewrite((cat):(dog)|(meow):(woof))")
    assert forward(woofify, "concatenate") == "condogenate"
    assert forward(woofify, "meowing") == "woofing"
    assert forward(woofify, "rodents") == "rodents"

def test_forward_unacceptable():
    give_me_a = FST.re("(a:c)*")
    assert forward(give_me_a, "aaa") == "ccc"
    with pytest.raises(AssertionError):
        forward(give_me_a, "no")

def test_forward_ambiguous():
    a_ears = FST.re("$^rewrite(a:[äáà])")
    assert forward(a_ears, "bcd") == "bcd"
    with pytest.raises(AssertionError):
        forward(a_ears, "cat")

def forward_n(fsts, input):
    """Returns all outputs when `fst` takes `input`."""
    fst = ff(fsts)
    return set(fst.generate(input))

def assert_preserves_phonotactic_validity(fsts):
    """Asserts that the given transducer preserves phonotactic validity."""
    fst = ff(fsts)
    # TODO: There are additional phonotactic constraints
    a = v9n.defs["S_aw"]
    b = FST.re("$^output($a @ $fst)", { 'a': a, 'fst': fst })
    b = v9n.simplify(b)
    violations = FST.re("$b - $a", { 'a': a, 'b': b })
    violations = v9n.simplify(violations)
    first_violation = next(violations.words(), None)
    if first_violation is not None:
        raise AssertionError(f"{fsts} does not preserve phonotactics: {first_violation}")
    # TODO: Check that fst is unambiguous

# Unit tests (rwplus)

def test_directional_application():
    assert forward(
        FST.re("$^rewrite2(a:'' / b _)", functions={rwplus.rewrite2}),
        "baaaaa"
    ) == "baaaa"
    assert forward(
        FST.re("$^rewrite2(a:'' / b _, dir = forward)", functions={rwplus.rewrite2}),
        "baaaaa"
    ) == "b"
    assert forward(
        FST.re("$^rewrite2(a:'' / b _, dir = backward)", functions={rwplus.rewrite2}),
        "baaaaa"
    ) == "baaaa"
    assert forward(
        FST.re("$^rewrite2(a:'' / _ c)", functions={rwplus.rewrite2}),
        "aaaaac"
    ) == "aaaac"
    assert forward(
        FST.re("$^rewrite2(a:'' / _ c, dir = forward)", functions={rwplus.rewrite2}),
        "aaaaac"
    ) == "aaaac"
    assert forward(
        FST.re("$^rewrite2(a:'' / _ c, dir = backward)", functions={rwplus.rewrite2}),
        "aaaaac"
    ) == "c"
    assert forward(
        FST.re("$^rewrite2('':a)", functions={rwplus.rewrite2}),
        "xxx"
    ) == "axaxaxa"
    assert forward(
        FST.re("$^rewrite2('':a, dir = forward)", functions={rwplus.rewrite2}),
        "xxx"
    ) == "axaxaxa"
    assert forward(
        FST.re("$^rewrite2('':a, dir = backward)", functions={rwplus.rewrite2}),
        "xxx"
    ) == "axaxaxa"
    assert forward(
        FST.re("$^rewrite2(a:b / _ a, dir = backward, rightmost = True)", functions={rwplus.rewrite2}),
        "aaa"
    ) == "aba"
    # Examples from Kaplan & Kay (1994)
    assert forward(
        FST.re("$^rewrite2(a:b / ab _ ba, dir = simultaneous)", functions={rwplus.rewrite2}),
        "abababababa"
    ) == "abbbbbbbbba"
    assert forward(
        FST.re("$^rewrite2(a:b / ab _ ba, dir = forward, leftmost = True)", functions={rwplus.rewrite2}),
        "abababababa"
    ) == "abbbabbbaba"
    assert forward(
        FST.re("$^rewrite2(a:b / ab _ ba, dir = backward, rightmost = True)", functions={rwplus.rewrite2}),
        "abababababa"
    ) == "ababbbabbba"
    # M’own examples again
    assert forward(
        FST.re("$^rewrite2(þ:t / _ (('-'? [aeiou] '-'?) & (.* '-' .*)) þ, dir = simultaneous)", functions={rwplus.rewrite2}),
        "þa-þ-eþ"
    ) == "ta-t-eþ"
    assert forward(
        FST.re("$^rewrite2(þ:t / _ (('-'? [aeiou] '-'?) & (.* '-' .*)) þ, dir = backward, rightmost = True)", functions={rwplus.rewrite2}),
        "þa-þ-eþ"
    ) == "þa-t-eþ"
    assert forward(
        FST.re("$^rewrite2(þ:t / _ (('-'? [aeiou] '-'?) & (.* '-' .*)) þ, dir = backward, rightmost = True)", functions={rwplus.rewrite2}),
        "þa-þ"
    ) == "ta-þ"
    assert forward(
        FST.re("$^rewrite2(þ:t / _ (('-'? [aeiou] '-'?) & (.* '-' .*)) þ, dir = backward, rightmost = True)", functions={rwplus.rewrite2}),
        "þaþ"
    ) == "þaþ"
    assert forward(
        FST.re("$^rewrite2(þ:(t '-') / _ (('-'? [aeiou] '-'?) & (.* '-' .*)) þ, dir = backward, rightmost = True)", functions={rwplus.rewrite2}),
        "þaþ"
    ) == "þaþ"

def test_rwplus_thydra():
    assert forward(
        FST.re("$^rewrite2(þ:(t '-') / _ a þ, dir = backward, rightmost = True)", functions={rwplus.rewrite2}),
        "þxþ"
    ) == "þxþ"
    assert forward(
        FST.re("$^rwplus(þ:(t '-') / _ a þ, dir = backward, rightmost = True)", functions={rwplus.rwplus}),
        "þxþ"
    ) == "þxþ"

def test_rwplus_single():
    assert forward(
        FST.re("$^rwplus(a:'' / b _)", functions={rwplus.rwplus}),
        "baaaaa"
    ) == "baaaa"
    assert forward(
        FST.re("$^rwplus(a:'' / b _, dir = forward)", functions={rwplus.rwplus}),
        "baaaaa"
    ) == "b"
    assert forward(
        FST.re("$^rwplus(a:'' / b _, dir = backward)", functions={rwplus.rwplus}),
        "baaaaa"
    ) == "baaaa"
    assert forward(
        FST.re("$^rwplus(a:'' / _ c)", functions={rwplus.rwplus}),
        "aaaaac"
    ) == "aaaac"
    assert forward(
        FST.re("$^rwplus(a:'' / _ c, dir = forward)", functions={rwplus.rwplus}),
        "aaaaac"
    ) == "aaaac"
    assert forward(
        FST.re("$^rwplus(a:'' / _ c, dir = backward)", functions={rwplus.rwplus}),
        "aaaaac"
    ) == "c"
    assert forward(
        FST.re("$^rwplus('':a)", functions={rwplus.rwplus}),
        "xxx"
    ) == "axaxaxa"
    assert forward(
        FST.re("$^rwplus('':a, dir = forward)", functions={rwplus.rwplus}),
        "xxx"
    ) == "axaxaxa"
    assert forward(
        FST.re("$^rwplus('':a, dir = backward)", functions={rwplus.rwplus}),
        "xxx"
    ) == "axaxaxa"
    assert forward(
        FST.re("$^rwplus(a:b / _ a, dir = backward, rightmost = True)", functions={rwplus.rwplus}),
        "aaa"
    ) == "aba"
    # Examples from Kaplan & Kay (1994)
    assert forward(
        FST.re("$^rwplus(a:b / ab _ ba, dir = simultaneous)", functions={rwplus.rwplus}),
        "abababababa"
    ) == "abbbbbbbbba"
    assert forward(
        FST.re("$^rwplus(a:b / ab _ ba, dir = forward, leftmost = True)", functions={rwplus.rwplus}),
        "abababababa"
    ) == "abbbabbbaba"
    assert forward(
        FST.re("$^rwplus(a:b / ab _ ba, dir = backward, rightmost = True)", functions={rwplus.rwplus}),
        "abababababa"
    ) == "ababbbabbba"
    # M’own examples again
    assert forward(
        FST.re("$^rwplus(þ:t / _ (('-'? [aeiou] '-'?) & (.* '-' .*)) þ, dir = simultaneous)", functions={rwplus.rwplus}),
        "þa-þ-eþ"
    ) == "ta-t-eþ"
    assert forward(
        FST.re("$^rwplus(þ:t / _ (('-'? [aeiou] '-'?) & (.* '-' .*)) þ, dir = backward, rightmost = True)", functions={rwplus.rwplus}),
        "þa-þ-eþ"
    ) == "þa-t-eþ"
    assert forward(
        FST.re("$^rwplus(þ:t / _ (('-'? [aeiou] '-'?) & (.* '-' .*)) þ, dir = backward, rightmost = True)", functions={rwplus.rwplus}),
        "þa-þ"
    ) == "ta-þ"
    assert forward(
        FST.re("$^rwplus(þ:t / _ (('-'? [aeiou] '-'?) & (.* '-' .*)) þ, dir = backward, rightmost = True)", functions={rwplus.rwplus}),
        "þaþ"
    ) == "þaþ"
    assert forward(
        FST.re("$^rwplus(þ:(t '-') / _ (('-'? [aeiou] '-'?) & (.* '-' .*)) þ, dir = backward, rightmost = True)", functions={rwplus.rwplus}),
        "þaþ"
    ) == "þaþ"

def test_rwplus_parallel():
    assert forward(
        FST.re("$^rwplus(a:b / _ a, b:c / _ b, c:d / _ c, dir = backward)", functions={rwplus.rwplus}),
        "aaccybbxaacbaa"
    ) == "badcycbxbadcba"
    assert forward(
        FST.re("$^rwplus(a:b / _ a, b:c / _ b, c:d / _ c, dir = backward)", functions={rwplus.rwplus}),
        "aaaccybbxaacbaa"
    ) == "abadcycbxbadcba"
    assert forward_n(
        FST.re("$^rwplus('':a, '':b)", functions={rwplus.rwplus}),
        "x"
    ) == {"axa", "axb", "bxa", "bxb"}
    # FIXME: actually gives {"a", "b", "c"} because the a:?b part always performs the rewrite, even though it might not lead to any effect
    # assert forward_n(
    #     FST.re("$^rwplus(a:?b, a:c)", functions={rwplus.rwplus}),
    #     "a"
    # ) == {"b", "c"}

def test_rwplus_directional_application_with_multiple_contexts():
    assert forward(
        FST.re("$^rwplus(x:y / a _ b, c _ d, dir = simultaneous)", functions={rwplus.rwplus}),
        "axb cxd axd cxb"
    ) == "ayb cyd axd cxb"
    assert forward(
        FST.re("$^rwplus(x:y / a _ b, c _ d, dir = outer)", functions={rwplus.rwplus}),
        "axb cxd axd cxb"
    ) == "ayb cyd axd cxb"
    # BUG: these two rules replace the last two instances of x because of broken context matching with directional modes
    # assert forward(
    #     FST.re("$^rwplus(x:y / a _ b, c _ d, dir = forward)", functions={rwplus.rwplus}),
    #     "axb cxd axd cxb"
    # ) == "ayb cyd axd cxb"
    # assert forward(
    #     FST.re("$^rwplus(x:y / a _ b, c _ d, dir = backward)", functions={rwplus.rwplus}),
    #     "axb cxd axd cxb"
    # ) == "ayb cyd axd cxb"

# Unit tests (v9n)

def test_sx():
    assert accepts("Sx_aw", "tan")
    assert accepts("Sx_aw", "fjor:nas")
    assert accepts("Sx_aw", "gcrîþ")
    assert accepts("Sx_aw", "van:ga")
    assert accepts("Sx_aw", "elt")
    assert accepts("Sx_aw", "a:ce:rin")
    # Sx also accepts noncanonical forms
    assert accepts("Sx_aw", "a:cer:in")
    # Needs explicit syllabification
    assert rejects("Sx_aw", "fjornas")
    # Whoops forgot a vowel
    assert rejects("Sx_aw", "a:ce:rn")
    # ŋ is no longer a valid coda
    assert rejects("Sx_aw", "tliŋ")
    # Hyphens not welcome to Sx
    assert rejects("Sx_aw", "fjor:n-as")
    # Eclipsed consonants only allowed word-initially
    assert rejects("Sx_aw", "va:ŋga")
    # Complex codas only allowed word-finally
    assert rejects("Sx_aw", "elt:ne")
    # Empty string is rejected
    assert rejects("Sx_aw", "")

def test_s():
    assert accepts("S_aw", "tan")
    assert accepts("S_aw", "fjor:nas")
    assert accepts("S_aw", "gcrîþ")
    assert accepts("S_aw", "van:ga")
    assert accepts("S_aw", "elt")
    assert accepts("S_aw", "a:ce:rin")
    assert accepts("S_aw", "a:cer:in")
    # Test hyphens
    assert accepts("S_aw", "fjor:n-as")
    # Multiple consecutive hyphens accepted
    assert accepts("S_aw", "fjor:n---as")
    # This might seem unintuitive, but the hyphen is actually adjacent to an empty onset or coda.
    assert accepts("S_aw", "-a")
    assert accepts("S_aw", "a-")
    # But these should not be accepted:
    assert rejects("S_aw", "-ma")
    assert rejects("S_aw", "an-")
    # Hyphen within multi-letter components rejected
    assert rejects("S_aw", "a:t-rân")
    assert rejects("S_aw", "on:d-el-t")
    # -: is invalid
    assert accepts("S_aw", "tan:-gen")
    assert rejects("S_aw", "tan-:gen")
    # But note that this places a hyphen before an empty coda:
    assert accepts("S_aw", "ta-:gen")
    # Test j
    assert accepts("S_aw", "fe:tj[-j]es")
    assert rejects("S_aw", "fe:tj[-j]jes")


def test_s_contains_sx():
    assert_contains("S_aw", "Sx_aw")


# def test_canonicalize_bridge():
#     assert forward("CanonicalizeBridge", "n:s") == "n:s"
#     assert forward("CanonicalizeBridge", "r:") == ":r"
#     assert forward("CanonicalizeBridge", "rþ:") == "r:þ"
#     assert forward("CanonicalizeBridge", "cþ:l") == "c:þl"
#     assert forward("CanonicalizeBridge", "s:l") == ":sl"
#     assert forward("CanonicalizeBridge", "rþ:cr") == "rþ:cr"

def test_repair_bridge():
    assert forward("RepairBridge", "c:-t") == "c:-t"
    assert forward("RepairBridge", "s:-m") == "s:-m"

    assert forward("RepairBridge", "t:-š") == ":-č-"
    assert forward("RepairBridge", "s:-h") == "s:-c-"
    assert forward("RepairBridge", "t:-ħ") == "-c:-d-"
    assert forward("RepairBridge", "t:-c·r") == "-c:-t·r-"
    assert forward("RepairBridge", "t:-cf") == ":-cf"
    assert forward("RepairBridge", "t:-gv") == ":-cf-"
    assert forward("RepairBridge", "t:-gð") == ":-cþ-"
    assert forward("RepairBridge", "c:-ŋ") == ":-ŋ"
    assert forward("RepairBridge", "t:-m") == "-n:-m"
    assert forward("RepairBridge", "þ:-ŋ") == "þ:-g-"
    assert forward("RepairBridge", "ł:-þ") == "ł:-t-"
    assert forward("RepairBridge", "ł:-d·l") == "ł:-dl-"
    assert forward("RepairBridge", "ł:-g") == "-l:-g"
    assert forward("RepairBridge", "þ:-þr") == ":-þr"
    assert forward("RepairBridge", "cþ:-þr") == "-c:-þr"
    assert forward("RepairBridge", "c:-gv") == ":-gv"
    assert forward("RepairBridge", "f:-tf") == "f:-t-"
    assert forward("RepairBridge", "rþ:-fl") == "-r:-fl"
    assert forward("RepairBridge", "ł:-") == "ł:-"
    assert forward("RepairBridge", "cþ:-") == "cþ:-"
    assert forward("RepairBridge", "þ:-s") == "þ:-þ-"
    assert forward("RepairBridge", "cþ:-s") == "cþ:-þ-"
    assert forward("RepairBridge", "s:-s") == ":-þ-"
    assert forward("RepairBridge", "s:-sr") == ":-þr-"
    assert forward("RepairBridge", "s:-ð·l") == ":-þl-"


def test_canonicalize_bridge_results_in_valid_bridge():
    # assert_contains(
    #     FST.re("$Kappa ':' $Iota", v9n.defs),
    #     FST.re("$^output($CanonicalizeBridge)", v9n.defs)
    # )
    assert_contains(
        # Bridge repair can result in the -ŋ pseudo-coda.
        FST.re("'-'? ($Kappa | ŋ) ':' '-'? $Iota '-'?", v9n.defs),
        FST.re("$^output($RepairBridge)", v9n.defs)
    )
    # # Assert that bridge repair does not introduce non-canonical bridges
    # assert_contains(
    #     FST.re("$^output($CanonicalizeBridge)", v9n.defs),
    #     FST.re("$^output($RepairBridge) & ($Kappa ':' $Iota)", v9n.defs)
    # )


def test_resolve_hyph_j():
    assert forward("ResolveHyphJ", "var:n-a") == "var:n-a"
    assert forward("ResolveHyphJ", "var:n[-j]e") == "var:n-je"
    assert forward("ResolveHyphJ", "fe:tj-a") == "fe:tj-a"
    assert forward("ResolveHyphJ", "fe:tj[-j]e") == "fe:tj-e"
    assert forward("ResolveHyphJ", "fe:tj-----a") == "fe:tj-a"
    assert forward("ResolveHyphJ", "fe:tj[-j][-j][-j][-j]e") == "fe:tj-e"
    assert_preserves_phonotactic_validity("ResolveHyphJ")


def test_remove_j_before_i():
    assert forward("RemoveJBeforeI", "var:n-a") == "var:n-a"
    assert forward("RemoveJBeforeI", "sa:jin") == "sa:in"
    assert forward("RemoveJBeforeI", "jî:mo") == "î:mo"
    assert forward("RemoveJBeforeI", "sa:ju:cin") == "sa:u:cin"
    assert forward("RemoveJBeforeI", "me:j-u") == "me:-u"
    assert_preserves_phonotactic_validity("ResolveHyphJ")


def test_has_oc():
    assert accepts("TypeIOC", "vi:-v")
    assert rejects("TypeIOC", "vi:v")
    assert rejects("TypeIOC", "-vi:v")
    assert rejects("TypeIOC", "vi:v-")
    assert rejects("HasTypeIOC", "ca:re:m-os")
    assert accepts("HasTypeIOC", "vi:-viþ")
    assert rejects("HasTypeIOC", "vi:v-iþ")
    assert accepts("HasTypeIIIOC", "va:na:g-a")
    assert rejects("HasTypeIIIOC", "va:na:ga")

def test_type_i_deduplication_medial():
    # Test rules for þ
    assert forward("DeduplicateTypeIOCMedial", "me:þ-oþ") == "me:-t-oþ"
    assert forward("DeduplicateTypeIOCMedial", "me:þoþ") == "me:þoþ"
    assert forward("DeduplicateTypeIOCMedial", "me:-þoþ") == "me:-þoþ"
    assert forward("DeduplicateTypeIOCMedial", "me:þo-þ") == "me:-to-þ"
    assert forward("DeduplicateTypeIOCMedial", "ve:cþ-aþ") == "vec:-t-aþ" # TODO: revisit (don’t preserve existing hyph?)
    assert forward("DeduplicateTypeIOCMedial", "vel:cþ-aþ") == "vel:-t-aþ"
    assert forward("DeduplicateTypeIOCMedial", "toþ:-eþ") == "to-t:-eþ"
    assert forward("DeduplicateTypeIOCMedial", "tocþ:-eþ") == "toc:-t-eþ" # TODO: revisit (don’t preserve existing hyph?)
    assert forward("DeduplicateTypeIOCMedial", "torþ:-eþ") == "tor:-t-eþ" # TODO: revisit (don’t preserve existing hyph?)
    assert forward("DeduplicateTypeIOCMedial", "to:t·-eþ") == "to:-t-eþ"
    assert forward("DeduplicateTypeIOCMedial", "to:þr-eþ") == "to:-tr-eþ"
    assert forward("DeduplicateTypeIOCMedial", "toþ:-reþ") == "to-t:-reþ"
    assert forward("DeduplicateTypeIOCMedial", "tocþ:-reþ") == "toc:-t-reþ"
    assert forward("DeduplicateTypeIOCMedial", "torþ:-reþ") == "tor:-t-reþ"
    assert forward("DeduplicateTypeIOCMedial", "to:t·r-eþ") == "to:-tr-eþ"
    # Test rules for ð
    assert forward("DeduplicateTypeIOCMedial", "ar:ð-e:ða") == "ar:-ŋ-e:ða"
    # Test proper application order
    assert forward("DeduplicateTypeIOCMedial", "a:þe:-þ-oþ") == "a:þe:-t-oþ"

# def test_oc_i_removed():
#     assert_disjoint("Applied", "TypeIOC")

# def test_oc_ii_removed():
#     assert_disjoint("Applied", "TypeIIOC")

# def test_oc_iii_removed():
#     assert_disjoint("Applied", "TypeIIIOC")
