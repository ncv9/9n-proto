# Typeset the main document
build:
    typst compile main.typ --font-path=fonts/ --ignore-system-fonts

# Watch for changes to the main document and typeset on each update
watch:
    typst watch main.typ --font-path=fonts/ --ignore-system-fonts --open

# Run tests for FST
[working-directory: 'f9d-proto']
test:
    uv run pytest test.py --capture=no --full-trace

# Invoke the magic mirror
[working-directory: 'f9d-proto']
magic-mirror:
    uv run magicmirror.py

# Open a REPL
[working-directory: 'f9d-proto']
repl:
    uv run python -i -c 'import pyfoma;from pyfoma import FST'
