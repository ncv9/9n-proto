use f9d_protato::components::*;
use rustfst::{prelude::SerializableFst, DrawingConfig};

fn main() {
    println!("Hello, world! {:#?}", *OMEGA);
    OMEGA
        .draw(
            "target/nu.dot",
            &DrawingConfig {
                acceptor: true,
                show_weight_one: false,
                print_weight: false,
                ..Default::default()
            },
        )
        .unwrap();
}
