//! cehas šinai; le afčias gralsar.

use rustfst::prelude::{optimize, TropicalWeight, VectorFst};
pub mod components;
pub mod symbols;

pub type Fst = VectorFst<TropicalWeight>;

pub fn simplify(mut fst: Fst) -> Fst {
    optimize(&mut fst).unwrap();
    fst
}
