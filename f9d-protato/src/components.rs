use std::sync::LazyLock;

use rustfst::{
    fst,
    prelude::{
        concat::ConcatFst,
        union::{union, UnionFst},
        MutableFst, TropicalWeight,
    },
    utils::acceptor,
    Semiring, Tr,
};
use strum::IntoEnumIterator;

use crate::{simplify, symbols::Symbol, Fst};

pub static DOTSTAR: LazyLock<Fst> = LazyLock::new(|| {
    let mut fst = Fst::new();
    let state = fst.add_state();
    fst.set_final(state, TropicalWeight::one()).unwrap();
    for symbol in Symbol::iter() {
        fst.add_tr(
            state,
            Tr::new(symbol as u32, symbol as u32, TropicalWeight::one(), state),
        )
        .unwrap();
    }
    fst
});

pub static MU: LazyLock<Fst> = LazyLock::new(|| simplify(opt(fst![Symbol::J as u32]).unwrap()));

pub static NU: LazyLock<Fst> = LazyLock::new(|| {
    use Symbol::*;
    simplify(any_of([E, O, A, Î, I, Ê, Ô, Â, U]).unwrap())
});

pub static MN: LazyLock<Fst> = LazyLock::new(|| simplify(seq(MU.clone(), NU.clone()).unwrap()));

pub static KAPPA: LazyLock<Fst> = LazyLock::new(|| {
    use Symbol::*;
    simplify(
        or_n([
            &opt(any_of([S, R, N, Þ, L, T, C, F, Ł]).unwrap()).unwrap(),
            &str([R, Þ]),
            &str([C, Þ]),
        ])
        .unwrap(),
    )
});

pub static OMEGA: LazyLock<Fst> = LazyLock::new(|| {
    use Symbol::*;
    simplify(
        or_n([
            &KAPPA,
            &str([S, T]),
            &str([L, T]),
            &str([N, S]),
            &str([L, S]),
            &str([N, Þ]),
            &str([L, T]),
            &str([M]),
        ])
        .unwrap(),
    )
});

pub static SIGMA0: LazyLock<Fst> = LazyLock::new(|| {
    use Symbol::*;
    simplify(any_of([C, N, Ŋ, V, S, Þ, R, L, Ł, M, F, G, P, T, Č, D, Ð, H, Ħ]).unwrap())
});

pub static SIGMA1: LazyLock<Fst> = LazyLock::new(|| {
    use Symbol::*;
    simplify(any_of([C·, V·, M·, F·, G·, P·, T·, Č·, D·, Ð·]).unwrap())
});

pub static SIGMA2: LazyLock<Fst> = LazyLock::new(|| {
    use Symbol::*;
    simplify(any_of([ND, ŊG, VF, VP, LŁ, MP, GC, DT, ÐÞ, TacrinarG]).unwrap())
});

pub static SIGMA_IOTA: LazyLock<Fst> =
    LazyLock::new(|| simplify(or(SIGMA0.clone(), SIGMA1.clone()).unwrap()));

pub static SIGMA_ALPHA: LazyLock<Fst> =
    LazyLock::new(|| simplify(or(SIGMA_IOTA.clone(), SIGMA2.clone()).unwrap()));

pub static PI0: LazyLock<Fst> = LazyLock::new(|| {
    use Symbol::*;
    simplify(any_of([C, V, S, Þ, Š, F, G, P, T, D, Ð, H, Ħ]).unwrap())
});

pub static PI_IOTA: LazyLock<Fst> = LazyLock::new(|| {
    simplify(
        and(
            SIGMA_IOTA.clone(),
            seq(PI0.clone(), DOTSTAR.clone()).unwrap(),
        )
        .unwrap(),
    )
});

pub static PI_ALPHA: LazyLock<Fst> = LazyLock::new(|| {
    use Symbol::*;
    simplify(
        or(
            PI_IOTA.clone(),
            any_of([ND, ŊG, VF, VP, MP, GC, DT, ÐÞ]).unwrap(),
        )
        .unwrap(),
    )
});

fn any_of(symbols: impl IntoIterator<Item = Symbol>) -> anyhow::Result<Fst> {
    let mut fst = Fst::new();
    let start = fst.add_state();
    let end = fst.add_state();
    fst.set_start(start)?;
    for symbol in symbols {
        fst.add_tr(
            start,
            Tr::new(symbol as u32, symbol as u32, TropicalWeight::one(), end),
        )?;
    }
    fst.set_final(end, TropicalWeight::one())?;
    Ok(fst)
}

fn str(symbols: impl IntoIterator<Item = Symbol>) -> Fst {
    let labels = symbols.into_iter().map(|s| s as u32).collect::<Vec<_>>();
    acceptor::<_, Fst>(&labels, Semiring::one())
}

fn opt(f: Fst) -> anyhow::Result<Fst> {
    UnionFst::<_, Fst>::new(f, fst![])?.compute()
}

fn or(f: Fst, g: Fst) -> anyhow::Result<Fst> {
    UnionFst::<_, Fst>::new(f, g)?.compute()
}

fn and(f: Fst, g: Fst) -> anyhow::Result<Fst> {
    todo!("https://github.com/garvys-org/rustfst/issues/250")
}

fn or_n<'a>(fsts: impl IntoIterator<Item = &'a Fst>) -> anyhow::Result<Fst> {
    let mut result = Fst::new();
    for fst in fsts {
        union(&mut result, fst)?;
    }
    Ok(result)
}

fn seq(f: Fst, g: Fst) -> anyhow::Result<Fst> {
    ConcatFst::<_, Fst>::new(f, g)?.compute()
}
