#import "template/template.typ"
#import template: conf, lang, nc, cv, part, mylink, l0, l1, l2w, l2, l3, term, St, Stx
#import "@preview/tablex:0.0.9": tablex, rowspanx, colspanx, hlinex, vlinex
#import "@preview/cetz:0.3.2"
#import "@preview/fletcher:0.5.5" as fletcher: diagram, node, edge

#let OC = [_#nc[o-?gi-?ni-?þe cfar-?ðerþ]_]

#show: conf()

// Title page

#page(
  fill: gradient.linear(rgb("#8af7f1"), rgb("#56e9f7"), angle: 90deg),
  margin: 0pt,
)[
  #context place(
    left + horizon,
    dx: 32pt,
    rotate(-82.5deg, reflow: true, box(inset: 32pt, width: 1.2 * page.height, fill: rgb("#ffffff"), align(center, text(font: "Libertinus Sans", size: 96pt, "9n-proto"))))
  )
  #place(
    right + bottom,
    dx: -48pt,
    dy: -48pt,
    box(width: 42%, fill: rgb("#ffffff66"), inset: 16pt, radius: 12pt, align(right, text(size: 24pt, [#set par(justify: false);A prototype grammar of Ŋarâþ Crîþ v9#text(fill: rgb("#f70455"))[n] \ #cv("+merlan #flirora") \ #text(style: "italic", "+merlan #flirora")])))
  )
]

// End title page

#outline(target: template.parts-and-headings)

= Introduction <ch:introduction>

This document is a prototype grammar of Ŋarâþ Crîþ v9n, the third revision of Ŋarâþ Crîþ v9.
Ŋarâþ Crîþ (Cenvos: #cv("ŋarâþ crîþ")) is a constructed language created by +merlan \#flirora (#cv("+merlan #flirora")).
Originally known as Necarasso Cryssesa in 2013, it has gone through several revisions since then.
Ŋarâþ Crîþ v7 was the first revision to be called Ŋarâþ Crîþ and revamped nearly every element of the language,
while Ŋarâþ Crîþ v9 elaborated further elements of the language.

Ŋarâþ Crîþ v9e was the first revision to the original version (v9c).
This edition uses a more principled approach to morphophonology developed in Project Elaine,
as well as sorting and refining the inflections of nouns, verbs, and relationals.
Since its release, it has been the current version of Ŋarâþ Crîþ v9.

Ŋarâþ Crîþ v9n is being worked on in order to fix unsatisfactory aspects of v9e.
This work was initially carried out as part of Project Shiva @shiva
to resolve the following issues:

- The deduplication rules are barebones and do not resolve all instances of
  objectionable repetition (#emph[#nc[oginiþe cfarðerþ]]).
- Assemblage types matter because of a flaw in coda resolution:
  #l0[reþ÷eþ] becomes #l1[reþeþ], while #l0[reþ:←eþ] becomes #l1[reteþ].
- The current inflection program, `f9i`, generates “all” inflected forms of
  each lexical entry in order to support searching by inflected form.
  Given the morphological richness of Ŋarâþ Crîþ, the resulting database
  takes an enormity of space.

Additional issues in scope for fixing in v9n include the following:

- Noun stems are allocated quite unevenly: the L stem is used for the locative,
  instrumental, and abessive cases, the last two of which are rarely used,
  and the S stem is used only for the fairly uncommon semblative case.
  In fact, it would make more sense for the accusative and dative cases to
  use different stems (where they currently both use N), since several verbs
  ascribe starkly different roles to these cases.
- The plural and especially dual numbers are rarely used in Ŋarâþ Crîþ v9
  because of the presence of collective nouns.
  They probably do not need a separate form for every case.
- Participle inflections differ substantially from noun inflections to avoid
  excessive rhyming. However, this difference complicates the language,
  perhaps needlessly.
- Final #l0[-f] is not as hyped as initially thought.
- It would be nice to describe Ŋarâþ Crîþ in itself.

The relational reform and the introduction of explicit conjunct forms were
initially planned for v9n but came out as later revisions to v9e.

This document is a #strong[prototype grammar].
It will put extra emphasis on differences from Ŋarâþ Crîþ v9e
and probably assume that you are familiar with it to some degree.
The grammar will also provide rationales on certain features when they differ from the previous version.
Conversely, content that will not change from v9e to v9n,
such as “Principles of Ŋarâþ Crîþ” and the “Background” part @gram,
are omitted from this document,
but they are no less important for the design of Ŋarâþ Crîþ v9n
than for Ŋarâþ Crîþ v9e.

For Cenvos text, this grammar uses the #emph[#nc[nafa vlina]] font developed in the early days of Ŋarâþ Crîþ v9.
A higher-quality Cenvos font is planned for the future.
However, we mostly stick to the romanization because
(1) #emph[#nc[nafa vlina]] does not support “flavored hyphens” and
(2) how many people actually know how to read Cenvos?


#part[Orthography and phonology]

Like the previous version of Ŋarâþ Crîþ, Ŋarâþ Crîþ v9n has the concept of orthographic and phonological #term[layers].

The most abstract layer is #term[layer 0], which encodes the morphophonemic representation of Ŋarâþ Crîþ.
Content in this layer exists structurally, as a path through a finite state machine.
In this grammar, text in layer 0 is written in double square brackets: #l0[tanc-a].

#term[Layer 1] is the graphemic representation, which is the most concrete ancestor
common to both the written and spoken modes.
It represents text after morphophonological processes such as bridge resolution and deduplication,
but it does not distinguish the final forms of #l1[c] or #l1[ŋ] or encode ligatures.
In this grammar, text in layer 1 is written in angle brackets: #l1[tanca].

#term[Layer 2w] is the surface glyphic representation.
It represents the sequence of Cenvos glyphs that is written,
observing required ligatures and final forms.
In this grammar, text in layer 2 is written with double angle brackets: #l2w[tanca];
for a more interesting example, #l1[mencoc] becomes #l2w[mencoc\$].

The remaining layers are outside the scope for this grammar.

= Layers 1 and 2w

This part is completely unchanged from v9e.

== Letters

#figure(
  tablex(
    columns: 12,
    auto-lines: false,
    align: (col, _row) => if calc.rem(col, 4) == 0 or calc.rem(col, 4) == 3 { right } else { left },
    hlinex(),
    ..(vlinex(), (), (), ()) * 3, vlinex(),
    ..([*Cen*], [*Name*], [*Rm*], [*Val*]) * 3,
    hlinex(),
    ..template.distr-cols(
      (
        ("c", "ca", 0),
        ("e", "e", 1),
        ("n", "na", 2),
        ("ŋ", "ŋa", 43),
        ("v", "va", 3),
        ("o", "o", 4),
        ("s", "sa", 5),
        ("þ", "þa", 85),
        ("š", "ša", 94),
        ("r", "ra", 6),
        ("l", "la", 7),
        ("ł", "ła", 119),
        ("m", "ma", 32),
        ("a", "a", 9),
        ("f", "fa", 10),
        ("g", "ga", 11),
        ("p", "pa", 12),
        ("t", "ta", 13),
        ("č", "ča", 222),
        ("î", "în", 14),
        ("j", "ja", 110),
        ("i", "i", 15),
        ("d", "da", 16),
        ("ð", "ða", 341),
        ("h", "ar", 17),
        ("ħ", "ħo", 18),
        ("ê", "ên", 257),
        ("ô", "ôn", 260),
        ("â", "ân", 265),
        ("u", "uħo", 19),
        ("w", "cełaŋa", -1),
        ("x", "avarte", -2),
        ("y", "priþnos", -3),
        ("z", "telrigjon", -4),
      ).map(((c, name, value)) => (cv(c + "~"), nc(name), nc(c), value)),
      4,
      3,
    ),
    hlinex(),
    ..template.distr-cols(
      (
        (cv[c], [], nc(overline[c\$]), []),
        (cv[ŋ], [], nc(overline[ŋ\$]), []),
        (cv[ee], [], nc(overline[ee]), []),
        (cv[em], [], nc(overline[em]), []),
        (cv[me], [], nc(overline[me]), []),
        (cv[mm], [], nc(overline[mm]), []),
        (cv[jâ], [], nc(overline[jâ]), []),
        (cv[âj], [], nc(overline[âj]), []),
        (cv[ww], [], nc(overline[ww]), []),
        (cv[xx], [], nc(overline[xx]), []),
        (cv[yy], [], nc(overline[yy]), []),
        (cv[zz], [], nc(overline[zz]), []),
      ),
      4,
      3,
    ),
    hlinex(),
    ..template.distr-cols(
      (
        (cv[\#], nc[carþ], nc[\#], 20),
        (cv[+], nc[tor], nc[+], 21),
        (cv[×], nc[njor], nc[+\*], 22),
        (cv[\@], nc[es], nc[\@], 23),
        (cv[\*], nc[nef], nc[\*], 25),
        (cv[\&], nc[sen], nc[\&], 26),
      ),
      4,
      3,
    ),
    hlinex(),
  ),
  kind: table,
  caption: [The letters of Ŋarâþ Crîþ.]
) <table:letters>

@table:letters shows the true letters, then the alternate forms and ligatures,
then the markers.

In layer 1, each letter is associated with an integer called the #term[letter number].
The sum of the letter numbers of a morpheme’s letters is called the #term[letter sum]
and is important in Ŋarâþ Crîþ morphology.

== Punctuation

#figure(
  tablex(
    columns: 9,
    auto-lines: false,
    align: (col, _row) => if calc.rem(col, 3) == 0 { right } else { left },
    hlinex(),
    ..(vlinex(), (), ()) * 3, vlinex(),
    ..([*Cen*], [*Name*], [*Rom*]) * 3,
    hlinex(),
    ..template.distr-cols(
      (
        (cv[.], nc[gen], nc[.]),
        (cv[;], nc[tja], nc[;]),
        (cv[?], nc[šac], nc[?]),
        (cv[!], nc[cjar], nc[!]),
        (cv[:], nc[vas], nc[:]),
        (cv[’], nc[ŋos], nc[’]),
        (cv[·], nc[łil], nc[·]),
        (cv[{], nc[rin], nc[{]),
        (cv[}], nc[cin], nc[}]),
        (cv[«], nc[fos], nc[«]),
        (cv[»], nc[þos], nc[»]),
        (cv[/], nc[jedva], nc[/]),
        (cv[-], nc[mivaf·ome], nc[-]),
      ),
      3,
      3,
    ),
    hlinex(),
  ),
  kind: table,
  caption: [The punctuation of Ŋarâþ Crîþ.]
) <table:punct>

@table:punct lists the punctuation characters of Ŋarâþ Crîþ.

== Digits

#figure(
  tablex(
    columns: 8,
    auto-lines: false,
    align: (col, _row) => if calc.rem(col, 2) == 0 { right } else { left },
    hlinex(),
    ..(vlinex(), ()) * 4, vlinex(),
    ..([*Cen*], [*\#*]) * 4,
    hlinex(),
    ..template.distr-cols(
      range(16).map(i => {
        let hex = template.hex(i)
        (cv(hex), hex)
      }),
      2,
      4,
    ),
    hlinex(),
  ),
  kind: table,
  caption: [The digits of Ŋarâþ Crîþ.]
) <table:digit>

= Phonotactics

A #term[morphological word] – that is, a word without any clitics – is made of
zero or more nonterminal syllables followed by one terminal syllable.
#footnote[Contrast morphological words with #term[phonological words], which includes clitics.]
Each syllable consists of four parts:
the #term[initial], the #term[glide], the #term[vowel], and the #term[coda].
The last syllable of a word differs from the other syllables in that
it allows for a wider selection of codas.
Likewise, the first syllable of a word differs from the other in that
it allows for an eclipsed consonant to be used as an initial.

Each of these syllable components affects what must come next for the rest of the word.
Therefore, it is useful to conceive of the structure of a word as a set of states,
with each type of component representing a transition between two of the states.
In particular, Ŋarâþ Crîþ can be thought of having six states:
#term[initial] ($alpha$), #term[syllabic] ($s$), #term[glide] ($g$), #term[onset] ($o$), #term[nuclear] ($n$), and #term[terminal] ($omega$).
#footnote[In v9e, Ŋarâþ Crîþ was described as having five states,
but this does not account for eclipsed consonants being restricted to word-initial position,
as well as the prohibitions on word-initial hyphens.
Note that eclipsed consonants are restricted to the start of a _phonological_ word;
that is, they cannot occur at the beginning of clitics.]

As shown in @fig:cþenros, each word begins in the initial state and ends in the terminal state.
An initial transitions from the syllabic state, which marks the start of the syllable,
to the glide state; a medial from the glide state to the onset state;
a vowel from the onset state to the nuclear state;
a simple coda from the nuclear state back to the syllabic state.
Alternately, a simple or complex coda can transition from the nuclear to the terminal state,
and an initial initial can transition from the initial to the glide state.

#figure(
  {
    let (a, s, g, o, n, w) = ((-1, 2), (1, 2), (0, 1), (1, 0), (2, 1), (3, 2))
    diagram(
      node-stroke: 1pt,
      node-shape: "circle",
      edge-stroke: 1pt,
      node(a, $alpha$),
      node(s, $s$),
      node(g, $g$),
      node(o, $o$),
      node(n, $n$),
      node(w, $omega$, extrude: (0, 3), outset: 3pt),
      edge(a, g, "->", $Alpha$, bend: 10deg),
      edge(s, g, "->", $Iota$, bend: 10deg),
      edge(g, o, "->", $Mu$, bend: 10deg),
      edge(o, n, "->", $Nu$, bend: 10deg),
      edge(n, s, "->", $Kappa$, bend: 10deg),
      edge(n, w, "->", $Omega$, bend: 10deg),
    )
  },
  caption: [A high-level view of Ŋarâþ Crîþ phonotactics. Note that the set names are all capital Greek letters.],
) <fig:cþenros>

Similarly, different types of morphemes can be derived by
starting on any one of the states in the diagram
and ending on another or the same state.
We denote the set of morphemes that on state $x$ and ends on state $y$ by $St(x, y)$.
For instance, full words in layer 0 are part of $St(alpha, omega)$.

In addition to producing a syllable component,
it is possible to produce a hyphen (#l0[-]) and stay in the current state,
as long as the current state is neither $alpha$ nor $omega$.
(That is, hyphens cannot occur word-initially or -finally.)
A hyphen denotes a point across which morphophonological changes can occur.
Given that some syllable components can be empty,
any path through the state machine is equivalent with another path
that results from exchanging a hyphen and an empty component.
That is, $("t"_Alpha, epsilon_Mu, "a"_Nu, epsilon_Kappa, "r"_Iota, "-", epsilon_Mu, "e"_Nu, epsilon_Omega)$
is equivalent to $("t"_Alpha, epsilon_Mu, "a"_Nu, epsilon_Kappa, "r"_Iota, epsilon_Mu, "-", "e"_Nu, epsilon_Omega)$
and should be treated as a superposition of both,
being able to morph into either one to satisfy morphophonological rules.#footnote[This principle of exchangability is a major deviation from Ŋarâþ Crîþ v9e,
in which, for example, #l0[reþ÷eþ] becomes #l1[reþeþ], while #l0[reþ:←eþ] becomes #l1[reteþ].
It was developed to prevent such unintuitive behavior,
as well as removing the need to use notational workarounds such as flavored hyphens.]
Multiple consecutive hyphens are also equivalent to one;
here, the form with the single hyphen in place of multiple is considered canonical.
$Stx(x, y)$ denotes the subset of $St(x, y)$ that excludes any hyphens.
For instance, phonotactically valid words in layer 1 are a part of $Stx(alpha, omega)$.

The character #l0[-#super[j]] works like a hyphen,
but it is usable only in the $o$ state and serves to add another #l0[j] to the result.
It is is resolved early during morphophonology.
Any group of consecutive #l0[-] or #l0[-#super[j]],
at least one of which is #l0[-#super[j]],
is equivalent to #l0[-#super[j]].

== General notation

State variables are denoted by $x$ and $y$.

Lowercase Greek letters denote variable strings; $epsilon$ denotes the empty string.
Capital Greek letters denote constant sets of strings.

$sigma[i]$ denotes the $i$th character of $sigma$, counting from zero.
$sigma[i..j]$ denotes the substring at the half-open range from $i$ to $j$, where both indices are counted from zero.

The concatenation of $sigma$ and $tau$ is denoted using juxtaposition as $sigma tau$.
Likewise, this can be extended to cases when one or both of the arguments is a set.

// TODO: review this
$:$ denotes a syllable boundary for disambiguation and is not a character _per se_.
However, implementations of Ŋarâþ Crîþ v9n morphophonology might prefer to introduce such a character
to avoid recalculating syllable boundaries repeatedly.

== Components of a syllable

The glide, vowel, and coda are simple to describe:

$
  Mu &= {epsilon, "j"} \
  Nu &= {"e", "o", "a", "î", "i", "ê", "ô", "â", "u"} \
  Kappa &= {epsilon, "c", "cþ", "n", "s", "þ", "r", "rþ", "l", "ł", "f", "t"} \
  Omega &= Kappa union {"ns", "nþ", "st", "ls", "lt", "łt", "m"}
$ <eq:simplecomps>

A consonant is any true letter that is neither a vowel nor a glide:

$
  Sigma_0 &= {"c", "n", "ŋ", "v", "s", "þ", "š", "r", "l", "ł", "m", "f", "g", "p", "t", "č", "d", "ð", "h", "ħ"}
$

Some of these can be lenited or eclipsed:

$
  Sigma_1 &= {"c·", "v·", "m·", "f·", "g·", "p·", "t·", "č·", "d·", "ð·"} \
  Sigma_2 &= {"nd", "ŋg", "vf", "vp", "lł", "mp", "gc", "dt", "ðþ", "g"'} \
  Sigma_iota &= Sigma_0 union Sigma_1 \
  Sigma_alpha &= Sigma_iota union Sigma_2
$

$"g"'$ denotes the result of eclipsing a vowel-initial word and is distinct from $"g"$.

Some consonants are designated as #term[effective plosives and fricatives]:

$
  Pi_0 &= {"c", "v", "s", "þ", "š", "f", "g", "p", "t", "d", "ð", "h", "ħ"} \
  Pi_iota &= {sigma in Sigma_iota mid(|) sigma[0] in Pi_0} \
  Pi_alpha &= Pi_iota union {"nd", "ŋg", "vf", "vp", "mp", "gc", "dt", "ðþ"}
$

#l0[r] and #l0[l] are considered #term[rhotics]:

$
  Rho &= {"l", "r"}
$

Lastly, there is a set of special initials:

$
  Lambda &= {"cs", "cþ", "cš", "cf", "gv", "gð", "tf", "dv"}
$

Now we can describe the set of all initials:

$
  Iota &= {epsilon} union Sigma_iota union (Pi_iota Rho) union Lambda \
  Alpha &= {epsilon} union Sigma_alpha union (Pi_alpha Rho) union Lambda
$

Observe that $Iota subset.neq Alpha$ and $Kappa subset.neq Omega$;
therefore, $St(s, y) subset.neq St(alpha, y)$ and $St(x, s) subset.neq St(x, omega)$.

Some terms are used to refer to a collection of syllabic components.
An #term[onset] is an initial followed by a glide.
A #term[bridge] is a coda followed by the onset of the following syllable:

$
  Gamma &= Kappa Iota
$

=== Bridge canonicality <sec:bridgecanon>

There is a further constraint on syllable structure: any bridges not separated by a hyphen must be #term[canonical].
That is, the initial must have as many consonants as possible:

$
  "*"("t"_Iota, epsilon_Mu, "a"_Nu, "n"_Kappa, epsilon_Iota, epsilon_Mu, "a"_Nu, epsilon_Omega)
  &-> ("t"_Iota, epsilon_Mu, "a"_Nu, epsilon_Kappa, "n"_Iota, epsilon_Mu, "a"_Nu, epsilon_Omega) \
  "*"("v"_Iota, "j"_Mu, "e"_Nu, "c"_Kappa, "þ"_Iota, epsilon_Mu, "o"_Nu, epsilon_Omega)
  &-> ("v"_Iota, "j"_Mu, "e"_Nu, epsilon_Kappa, "cþ"_Iota, epsilon_Mu, "o"_Nu, epsilon_Omega) \
  "*"("s"_Iota, epsilon_Mu, "i"_Nu, "cþ"_Kappa, "r"_Iota, epsilon_Mu, "e"_Nu, epsilon_Omega)
  &-> ("s"_Iota, epsilon_Mu, "i"_Nu, "c"_Kappa, "þr"_Iota, epsilon_Mu, "e"_Nu, epsilon_Omega)
$

Inputs with noncanonical bridges are considered to be invalid in layer 0 in the first place,
but if one wishes to accept noncanonical input,
then one can run a canonicalization step before the rest of the morphophonological process.
Similarly, if the input is not explicitly syllabified,
then one would need to syllabify the input according to the maximal onset principle.

Note that a hyphen defers canonicalization of bridges, so that
$("v"_Iota, "j"_Mu, "e"_Nu, "c"_Kappa, "-", "þ"_Iota, epsilon_Mu, "o"_Nu, epsilon_Omega)$
is valid in layer 0; the canonicalization occurs when converting to layer 1.

=== Additional constraints

Neither #l0[j] nor #l0[-#super[j]] is allowed before #l0[î], #l0[i], or #l0[u]. As with bridge canonicality, this constraint applies only when the two components are not separated by a hyphen.

=== Markers

Markers exist on a separate plane from the rest of the word.
Since they denote semantic rather than phonemic information,
they do not participate in applying morphophonological rules;
for instance, prefixes added to a stem containing a marker
cause the marker to fall before the first prefix.
Nevertheless, they are important in determining inflections
because they have letter values.

The markers on a word are collectively known as #term[decoration].
The decoration before the rest of the word consists of three parts:
an optional nef, an optional name marker, and an optional sen:

$
  Psi &= {epsilon, "*"} {epsilon, "#", "+", "+*", "@"} {epsilon, "&"}
$

To complicate matters, markers can also apply over multiple words.
If this happens, then the letter values of the markers are not counted toward the inflection of any of the inner words.

= Morphophonological rules

In contrast to Ŋarâþ Crîþ v9e, all hyphens are resolved at once instead of being joined serially from start to end.
This change is intended to facilitate FST-based approaches to applying these rules.

== Objectives of the phonological rules

The objectives of Ŋarâþ Crîþ v9n’s phological rules are to:

1. Always produce a single phonotactically valid output
2. Always output canonical bridges
3. Never output #OC if it was absent within any of the input’s morphemes

== Bridge repair

In addition to being noncanonical, bridges separated by a hyphen might be awkward to pronounce.
#term[Bridge repair] is a process that makes such bridges easier to pronounce.
It occurs before the resolution of #OC and thus can accept or generate noncanonical bridges.

A #term[valid] bridge is one that can be output from bridge repair.
All bridges #emph[within] a morpheme are canonical, but most are also valid.

Bridge repair is not idempotent: for instance, #l0[s:-ð] yields #l0[s:s], but #l0[s:-s] yields #l0[:þ].

== Deduplication and #OC

Certain types of phonetic duplication are considered unpleasant in Ŋarâþ Crîþ and are called #term[#OC].
These are divided into three types:

/ Type I: Two equivalent consonants flanking a vowel.
/ Type II: Two consecutive closed rimes differing only by vowel tone (i.e. having the same vowel quality and coda).
/ Type III: Three consecutive vowels of the same quality.

#OC is resolved only if it was absent in any of the morphemes to begin with.
For instance, a word such as #l0[viv-iþ], which contains #OC entirely within the first morpheme,
stays as #l1[viviþ] in layer 1.

=== Type I OC

Consonants are divided into classes depending on the environment that constitutes Type I #OC (where V includes an optional glide):

/ Class I+: CVC, CrVC, and CVrC constitute #OC – #l0[þ ð].
/ Class I: CVC constitutes #OC – #l0[c ŋ v š m f g p č d h ħ].
/ Class II: CVCV or VCVC (not necessarily with the same vowel quality) constitutes #OC – #l0[n s l t].#footnote[Formerly, the vowels were required to have the same quality, but the automaton to detect this was too complicated.]
/ Class III: Resolved when converting from layer 2s to 3s and thus never constitute #OC – #l0[r ł].

Mutated consonants play a double role as both the original and (if present) sounding consonant.
For example, #l0[termo-m·eða] constitutes #OC between the #l0[m] and #l0[m·],
as does #l0[miva-m·eða] between the #l0[v] and #l0[m·].
This double role applies even when both consonants are mutated from different base consonants,
so #l0[vf] and #l0[m·] could constitute #OC as #l0[v]-like consonants,
as could #l0[v·] and #l0[m·] (the former as the base consonant; the latter as the sounding consonant).
However, note that, for example, #l0[f·] and #l0[ð·] do not constitute #OC with each other,
as they have no sounding consonant at all.

To put it another way, we have a set of #l0[c]-like consonants, #l0[n]-like consonants, and so on, some of which might not be disjoint with others.
We denote one of these sets as $cal(K)(sigma)$.
Conversely, the set of base consonants that a possibly mutated consonant acts as is denoted as $cal(K)^<- (sigma)$.

This fact also implies that an initial mutation applied to a word might cause #OC without further intervention;
for example, leniting #l0[mêva] would cause #OC with #l0[m·] and #l0[v].

The grammar rules resolve #OC by changing either the first or second occurrence of the repeated consonant to another consonant.
In particular, the second occurrence is changed if the #OC occurs word-initially;
otherwise, the first occurrence is the one usually changed.

Resolving one instance of #OC by changing a consonant might create another instance of #OC where there was none before.
When the first occurrence of the repeated consonant is altered,
the replacement might form #OC with the preceding consonant across a vowel.

=== Type II OC

=== Type III OC

== Formal description

#let arrowh = math.accent(sym.arrow.r, math.macron)

As usual, $alpha -> beta \/ lambda "_" rho$ represents a replacement of $alpha$ with $beta$ when between $lambda$ and $rho$,
with the postfix being omitted for unconditional substitutions.
$xi => alpha -> beta \/ lambda "_" rho$ represents a replacement of $alpha(xi)$ with $beta(xi)$
and is used when the output depends on the input.

The identity function is denoted by $"id"$.

$sigma^?$, $sigma^*$, and $sigma^+$ denote the occurrence of zero or one, zero or more, or one or more occurrences of $sigma$, respectively.

$alpha arrowh beta \/ lambda "_" rho$ is similar, but…

=== Resolving #l0[-#super[j]]

$
  "-"^j &-> "-" && \/ "j" "_" \
  "-"^j &-> "-" "j"
$

=== Removal of #l0[j] before certain vowels

$
  "j" &-> epsilon && \/ "_" "-"^? \{"î", "i", "u"\}
$

=== Bridge resolution

=== Blah blah blah

=== Hyphen removal

At the end of the process, all hyphens are removed:

$
  "-" &-> epsilon
$

#part[Morphology]

#part[Syntax]

#part[Lexicon]

#bibliography("data/works.yml", full: true)

// Things to do at the end of the document
