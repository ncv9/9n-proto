import json
from pathlib import Path
import subprocess

result = subprocess.run(
    ["typst", "query", "main.typ", "<crisotasa>", "--font-path=fonts/", "--field", "value", "--one"],
    capture_output=True,
    encoding="utf-8",
)

result.check_returncode()
for fname, code in json.loads(result.stdout).items():
    fpath = Path(fname)
    fpath.parent.mkdir(parents=True, exist_ok=True)
    with fpath.open("w") as fh:
        fh.write(code)
