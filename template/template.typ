#let distr(l, k) = {
  let n = l.len()
  let q = calc.div-euclid(n, k)
  let r = calc.rem-euclid(n, k)
  if r > 0 { q += 1 }
  let j = 0
  for i in range(k) {
    if j < n { (l.slice(j, calc.min(j + q, n),),) } else { ((),)}
    j += q
  }
}

#let distr-cols(entries, width, cols) = {
  let distr-entries = distr(entries, cols)
  let rows = distr-entries.at(0).len()
  for rownum in range(rows) {
    for colnum in range(cols) {
      let current-col = distr-entries.at(colnum)
      if rownum < current-col.len() {
        current-col.at(rownum)
      } else {
        (none,) * width
      }
    }
  }
}

#let latin-to-cenvos = json("../data/mapping.json")

#let parts-and-headings = figure.where(kind: "part", outlined: true).or(heading.where(outlined: true))

// TODO: replace with custom element once we get them
#let part(c) = {
  figure(
    kind: "part",
    numbering: "I",
    supplement: "Part",
    caption: [],
    c,
  )
}

#let lang(lang, c) = text(lang: lang, c)
#let nc(c) = lang("art", c)
#let l0(c) = [⟦#nc(c)⟧]
#let l1(c) = [⟨#nc(c)⟩]
#let l2w(c) = [²⟨#nc(c)⟩]
#let l2(c) = [/#c/]
#let l3(c) = [\[#c\]]

#let term(c) = text(tracking: 0.02em, smallcaps(c))

#let St(x, y) = $cal(S)_(#x #y)$
#let Stx(x, y) = $cal(S)^*_(#x #y)$

#let transform-string-to-cenvos(s) = {
  let t = s.replace("+*", "×")
  for char in t {
    latin-to-cenvos.at(char, default: char)
  }
}

#let transform-to-cenvos(c) = {
  [#show regex(".+"): it => {
    if it.has("text") { transform-string-to-cenvos(it.text) }
    else { it }
  };#c]
}

#let cv(c) = text(lang: "art", script: "qacv", transform-to-cenvos(c))

#let mylink(dest, c) = [#c#footnote(link(dest))]

#let show-part(part) = {
  [
    #set text(font: "Libertinus Sans", weight: "bold")
    #set par(justify: false)
    #pagebreak(to: "even")
    #v(72pt)
    #align(right)[
      #text(size: 36pt, counter(figure.where(kind: "part")).display("I"))
      #v(-24pt)
      #line(length: 100%)
      #v(-24pt)
      #text(size: 48pt, part.body)
    ]
    #v(12pt)
  ]
}

#let hex(n) = upper(str(n, base: 16))
#let hexcv(n) = cv(hex(n))

#let show-chapter(chapter) = {
  let ordinal = if chapter.numbering == none {
    none
  } else {
    counter(heading).get().at(0)
  }
  let ordinal-hex = if ordinal == none {
    none
  } else {
    hexcv(ordinal)
  }
  [
    #set text(font: "Libertinus Sans", weight: "bold", size: 26pt)
    #set par(justify: false)
    #pagebreak()
    #block[
      #align(
        horizon,
        stack(
          dir: ltr,
          spacing: 0.5em,
          [#ordinal],
          line(angle: 90deg, length: 1.5em),
          chapter.body,
          1fr,
        ),
      )
      #place(bottom + right, dy: -4pt, text(size: 56pt, fill: luma(0%, 24%), ordinal-hex))
    ]
    #v(12pt)
  ]
}

#let show-section(section) = {
  let size = if section.level == 2 { 20pt }
      else if section.level == 3 { 16pt }
      else { 14pt }
  [
    #set text(font: "Libertinus Sans", weight: "bold", size: size)
    #set par(justify: false)
    #block[
      #align(
        horizon,
        stack(
          dir: ltr,
          spacing: 0.5em,
          counter(heading).display(),
          line(angle: 90deg, length: 1em),
          section.body,
          1fr,
        ),
      )
    ]
  ]
}

#let show-outline-entry(it) = {
  if it.element.func() == figure {
    let res = link(it.element.location(), 
      if it.element.numbering != none {
        numbering(it.element.numbering, ..it.element.counter.at(it.element.location()))
      } + [ ] + it.element.body
    )

    if it.fill != none {
      res += [ ] + box(width: 1fr, it.fill) + [ ] 
    } else {
      res += h(1fr)
    }

    res += link(it.element.location(), it.page())
    res += linebreak()
    strong(res)
  } else {
    move(box(it, width: 100% - 1em), dx: 1em)
  }
}

#let conf() = (doc) => {
  set page(footer: context {
    let pageno = counter(page).get().at(0)
    stack(
      dir: if calc.rem(pageno, 2) == 0 { ltr } else { rtl },
      [#pageno], h(1fr), [#hexcv(pageno)]
    )
  })

  set heading(
    numbering: "1.1.1",
    supplement: it => {
      ("Part", "Chapter").at(it.depth, default: "Section")
    },
  )
  show outline: set heading(numbering: none)
  show figure.where(kind: "part"): show-part
  show heading.where(level: 1): show-chapter
  show heading.where(level: 2): show-section
  show heading.where(level: 3): show-section
  show heading.where(level: 4): show-section
  show outline.entry: show-outline-entry

  set text(font: "Libertinus Serif", features: ("ss07", "liga", "hlig"), fallback: false)
  // nafa vlina encodes its characters in the Old Hungarian block to get
  // RTL directionality, so we have to set the script to "hung" to get
  // kerning and ligatures.
  // TODO: develop a better font for Cenvos
  show text.where(script: "qacv"): set text(font: "NafaVlina", script: "hung")
  show raw: set text(font: "Fira Code", fallback: false)
  set par(justify: true)

  set math.equation(numbering: "(1)")
  show math.equation: set text(font: "Libertinus Math", fallback: false)
  
  doc
}
