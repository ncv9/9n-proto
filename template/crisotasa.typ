/*

Node format:

- literals: A list of $k + 1$ strings denoting literal code
- placeholders: A list of $k$ node indices denoting child placeholders.
    Contains `none` for placeholders yet to be resolved.

State format:

- nodes: A list of nodes that have been created.
- files: A map from file names to node indices describing the root node for each output file.
- placeholders: A map from placeholder names to (node index, placeholder index) pairs
    describing where to find the placeholder.

*/

// The global state holding the stored code.
#let weave-state = state(
  "weave-state",
  (
    nodes: (),
    files: (:),
    placeholders: (:),
  )
)

// The pattern for titles.
#let title-pattern = regex("◊◊(◊?) (.+)\n")

// The pattern for placeholders.
#let placeholder-pattern = regex("(?m)^◊ (.+)\n")

// Given a string, returns an (is-file, name, literals, child-names) tuple,
// or `none` if the code block should be ignored.
#let parse-raw(s) = {
  if not s.ends-with("\n") {
    s += "\n"
  }
  let title-match = s.match(title-pattern)
  if title-match == none {
    return none
  }
  let (sym, name) = title-match.captures
  let is-file = sym.len() > 0
  s = s.slice(title-match.end)
  let literals = ()
  let children-map = (:)
  let children = ()
  let matches = s.matches(placeholder-pattern)
  let index = 0
  for match in matches {
    let literal = s.slice(index, match.start)
    let (child,) = match.captures
    if child == "..." {
      child = name
    }
    if child in children-map {
      panic("Duplicate placeholder " + child)
    }
    literals.push(literal)
    children.push(child)
    children-map.insert(child, true)
    index = match.end
  }
  let rest = s.slice(index)
  literals.push(rest)
  (is-file, name, literals, children)
}

// Updates the weave state based on a string from a code block.
#let process(s) = {
  let res = parse-raw(s)
  if res == none {
    return
  }
  let (is-file, name, literals, children) = res
  weave-state.update(weave-state => {
    let new-node-id = weave-state.nodes.len()
    if is-file {
      if name in weave-state.files {
        panic("Duplicate file " + name)
      }
      weave-state.files.insert(name, new-node-id)
    } else {
      let (node-index, placeholder-index) = weave-state.placeholders.remove(name, default: (none, none))
      if node-index == none {
        panic("Unrecognized symbol " + name)
      }
      weave-state.nodes.at(node-index).placeholders.at(placeholder-index) = new-node-id
    }
    weave-state.nodes.push((
      literals: literals,
      placeholders: (none,) * children.len(),
    ))
    for (i, child) in children.enumerate() {
      if child in weave-state.placeholders {
        panic("Duplicate placeholder " + child)
      }
      weave-state.placeholders.insert(child, (new-node-id, i))
    }
    weave-state
  })
}

// Weaves the node with a given index.
#let weave-node(node-id) = {
  let weave-state = weave-state.get()
  let node = weave-state.nodes.at(node-id)
  for (i, placeholder) in node.placeholders.enumerate() {
    node.literals.at(i)
    weave-node(placeholder)
  }
  node.literals.at(-1)
}

#let weft() = {
  let weave-state = weave-state.get()
  if weave-state.placeholders.len() > 0 {
    panic("Unresolved placeholders: " + weave-state.placeholders.keys().join(", "))
  }
  let weaved = (:)
  for (name, node-id) in weave-state.files {
    weaved.insert(name, weave-node(node-id))
  }
  weaved
}

// Finishes weaving and outputs the code as metadata.
#let finish-weave() = {
  [#metadata(weft()) <crisotasa>]
}

#let show-weave() = {
  it => {
    process(it.text)
    show raw.line: line => {
      if line.text.starts-with("◊") {
        text(font: "Libertinus Serif", style: "italic", line.text)
      } else {
        line
      }
    }
    if it.block {
      block(width: 100%, stroke: 0.2pt, inset: 8pt, fill: rgb(70%, 95%, 100%, 15%), it)
    } else {
      it
    }
  }
}
